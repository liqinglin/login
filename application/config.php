<?php
use think\Env;

return [
//     'app_status'             => Env::get('coobar_scenario', ''),
    'default_return_type'    => 'json',
    'url_common_param'       => true,
     'exception_handle'       => '\\app\\core\\ExceptionBase',

    'session' => [
        'auto_start'     => true,
    ],

    'log'   => [
        'type'          => 'file',
        //错误的日志 单独处理
        'error_path' => '/data/log/login_error_log/',
    ],
    //API版本号
//     'api_version'       => ['06000000','06000100'],
    //过滤掉不受版本控制的模块
//     'api_version_filter_module' => ['oauth','test','html','callback'],
    //IP限制 支持通配符
    'allowedIPs' => '',

    //自定义日志目录
    'coobar_log' => [
        'default'   => '/data/log/login/',
        'http'      => '/data/log/http_login/',
    ],

    'SMS_LINE_LIST' => array(
        'JNB'  => '纪念币交易网',
        'QH'   => '琴海文交中心',
        'JCWH' => '集邮爱好者之家',
        'QHYX' => '票友汇',
        'SEMB' => '快乐贸'
    ),

    'SMS_CONFIG' => array(
        //每日最多申请次数
        'MAX_DAY_APPLY_COUNTS' => 3,
        //验证码有效期(秒)
        'MOB_CODE_LIFE'        => 60*15,
        //验证码申请间隔时间
        'MOB_REQUEST_INTERVAL' => 60,
        'APIURL'               => 'http://sms.eloone.com/ylSend.do',
        /*
         'USERNAME' => 'sy477',
         'PASSWORD' => '789789',
        */
        'USERNAME' => 'xsjjnb',
        'PASSWORD' => '123123',
        'SND'      => ''
    ),

    'SMS_CONFIG_SEMB' => array(
        //每日最多申请次数
        'MAX_DAY_APPLY_COUNTS' => 3,
        //验证码有效期(秒)
        'MOB_CODE_LIFE' => 300,
        //验证码申请间隔时间
        'MOB_REQUEST_INTERVAL' => 60,
        'APIURL' => 'http://202.91.244.252/qd/SMSSendYD',
        /* 纪念币交易网 */
        'USERNAME' => '863008',
        'PASSWORD' => '110110',
        'SND' => ''
    ),

    //普讯接口
    'SMS_CONFIG_JNB' => array(
        //每日最多申请次数
        'MAX_DAY_APPLY_COUNTS' => 3,
        //验证码有效期(秒)
        'MOB_CODE_LIFE' => 300,
        //验证码申请间隔时间
        'MOB_REQUEST_INTERVAL' => 60,
        'APIURL' => 'http://202.91.244.252/qd/SMSSendYD',
        /* 纪念币交易网 */
        'USERNAME' => '863',
        'PASSWORD' => 'jnb@863gl',
        'SND' => ''
    ),

    //普讯接口
    'SMS_CONFIG_QH' => array(
        //每日最多申请次数
        'MAX_DAY_APPLY_COUNTS' => 3,
        //验证码有效期(秒)
        'MOB_CODE_LIFE' => 300,
        //验证码申请间隔时间
        'MOB_REQUEST_INTERVAL' => 60,
        'APIURL' => 'http://202.91.244.252/qd/SMSSendYD',
        /* 纪念币交易网 */
        //'USERNAME' => '863',
        //'PASSWORD' => 'jnb@863gl',
        /* 琴海 */
        'USERNAME' => '863001',
        'PASSWORD' => 'qhwj@863qd',
        'SND' => ''
    ),

    'SMS_CONFIG_QHYX' => array(
        //每日最多申请次数
        'MAX_DAY_APPLY_COUNTS' => 3,
        //验证码有效期(秒)
        'MOB_CODE_LIFE' => 300,
        //验证码申请间隔时间
        'MOB_REQUEST_INTERVAL' => 60,
        'APIURL' => 'http://202.91.244.252/qd/SMSSendYD',
        /* 集藏文化 */
        'USERNAME' => '863005',
        'PASSWORD' => 'cncoin.cn',
        'SND' => ''
    ),

    'SMS_CONFIG_JCWH' => array(
        //每日最多申请次数
        'MAX_DAY_APPLY_COUNTS' => 3,
        //验证码有效期(秒)
        'MOB_CODE_LIFE' => 300,
        //验证码申请间隔时间
        'MOB_REQUEST_INTERVAL' => 60,
        'APIURL' => 'http://202.91.244.252/qd/SMSSendYD',
        /* 集藏文化 */
        'USERNAME' => '863002',
        'PASSWORD' => '110110..',
        'SND' => ''
    ),

    'SMS_CONFIG_XSJ' => array(
        //每日最多申请次数
        'MAX_DAY_APPLY_COUNTS' => 3,
        //验证码有效期(秒)
        'MOB_CODE_LIFE' => 300,
        //验证码申请间隔时间
        'MOB_REQUEST_INTERVAL' => 60,
        'APIURL' => 'http://202.91.244.252/qd/SMSSendYD',
        /* 集藏文化 */
        'USERNAME' => '863004',
        'PASSWORD' => 'xsj@4',
        'SND'      => ''
    ),

    'USER_SESSION'        => 'user_info',
    'GET_PWD_UID_SESSION' => 'get_pwd_uid',
    'TRACK_SESSION'       => 'track_uid',

    'LOGIN_URL'     => 'http://login.semstq.com/',
    'RETAIL_URL'    => 'http://shop.semstq.com/',
    'HAPPY_MAO_URL' => 'http://happymao.semstq.com/',
];

