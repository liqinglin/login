<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/1
 * Time: 9:31
 */
namespace app\core;

use app\core\observer\RequestObserver;
use app\core\observer\SessionObserver;
use SplObserver;
use app\core\observer\OauthObserver;

class Base implements \SplSubject
{
    use traits\controller\Jump;
    private $_storage = null;

    public function __construct()
    {
        $this->attach(new OauthObserver());
        $this->attach(new SessionObserver());
        $this->attach(new RequestObserver());
        $this->notify();
    }

    private function _getStorage()
    {
        if(is_null($this->_storage)) {
            $this->_storage = new \SplObjectStorage();
        }
        return $this->_storage;
    }

    /**
     * Attach an SplObserver
     * @link http://php.net/manual/en/splsubject.attach.php
     * @param SplObserver $observer <p>
     * The <b>SplObserver</b> to attach.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function attach(\SplObserver $observer)
    {
        $this->_getStorage()->attach($observer);
    }

    /**
     * Detach an observer
     * @link http://php.net/manual/en/splsubject.detach.php
     * @param SplObserver $observer <p>
     * The <b>SplObserver</b> to detach.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function detach(\SplObserver $observer)
    {
        $this->_getStorage()->detach($observer);
    }

    /**
     * Notify an observer
     * @link http://php.net/manual/en/splsubject.notify.php
     * @return void
     * @since 5.1.0
     */
    public function notify()
    {
        $storage = $this->_getStorage();
        foreach($storage as $row) {
            $row->update($this);
        }
    }
}