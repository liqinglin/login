<?php
namespace app\core\traits\controller;

use think\Response;
use think\exception\HttpResponseException;
use think\Config;
use app\core\error\Error;

trait Jump
{

    public function error($error)
    {
        $params = func_get_args();
        unset($params[0]);

        $error = Error::trans($error, $params);
        $error['status'] = '0';
        $response = Response::create($error, 'json', 200, [], []);
        throw new HttpResponseException($response);

    }

    public function success($data = array())
    {
        $response = Response::create(array('status' => '1', 'data' => (object)$data), 'json', 200, [], []);
        throw new HttpResponseException($response);
    }
}