<?php
namespace app\core\traits\controller;

use think\Request;
use think\Response;
use think\exception\HttpResponseException;
use think\Config;
use app\core\error\Error;

trait RestJump
{
    private $_defaultCode = 200;
    private static $_code = array(
        'get' => 200,
        'post' => 201,
        'put' => 201,
        'delete' => 201,
    );
    public function error($error)
    {
        $params = func_get_args();
        unset($params[0]);
        $error = Error::trans($error, $params);
        $error['status'] = '0';
        $method = strtolower(Request::instance()->method());
        $code = $this->_defaultCode;
        if(array_key_exists($method,self::$_code)) {
            $code = self::$_code[$method];
        }
        $response = Response::create($error, 'json', $code, [], []);
        throw new HttpResponseException($response);

    }

    public function success($data = array())
    {
        $method = strtolower(Request::instance()->method());
        $code = $this->_defaultCode;
        if(array_key_exists($method,self::$_code)) {
            $code = self::$_code[$method];
        }
        $response = Response::create(array('status' => '1', 'data' => (object)$data), 'json', $code, [], []);
        throw new HttpResponseException($response);
    }
}