<?php
/**
 * 回调观察者
 * User: honglvshi
 * Date: 2016/11/8
 * Time: 18:17
 */
namespace app\core\observer;
use app\core\error\SessionError;
use think\Controller;
use think\Request;
use think\Session;

class SessionObserver extends Controller implements \SplObserver
{
    private $_skipSession = [
    ];

    /**
     * Receive update from subject
     * @link http://php.net/manual/en/splobserver.update.php
     * @param SplSubject $subject <p>
     * The <b>SplSubject</b> notifying the observer of an update.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function update(\SplSubject $subject)
    {
        return true;
//         Session::boot();
        $moduleName = Request::instance()->module();
        $controllerName = Request::instance()->controller();
        $name = strtolower($moduleName . '-' . $controllerName);
        if(in_array($name,$this->_skipSession)) {
            return true;
        }
        if(Session::has('user_info.uid')) {
            return true;
        }
        return $subject->error(SessionError::$SESSION_IS_OUT_TIME);
    }
}