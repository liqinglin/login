<?php
/**
 * 授权观察者 检查token
 * User: honglvshi
 * Date: 2016/11/2
 * Time: 14:35
 */
namespace app\core\observer;
use app\core\error\OauthError;
use app\core\helper\IpHelper;
use think\Config;
use think\Request;
use app\core\error\Error;
use app\data\dbcache\model\prime\OauthCache;

class OauthObserver implements \SplObserver
{
    public function update(\SplSubject $subject)
    {
        $this->_oauthIp($subject);
        return true;
    }

    public function _oauthIp(\SplSubject $subject)
    {
        $ipAddress = Request::instance()->ip();
        $oauthAppid = Request::instance()->param('oauth_appid');
        $oauthServer = self::getServer($oauthAppid);
        if(empty($oauthServer)) {
            $subject->error(OauthError::$OAUTH_SERVER_IS_NOT_FIND);
        }
        $allowIP = Config::get('allowedIPs');
        $result = IpHelper::checkAccessIp($ipAddress,$allowIP);
        if(false === $result) {
            $subject->error(Error::explain(OauthError::$OAUTH_IP_IS_NOT_ALLOWED,[$ipAddress]));
        }
        return true;
    }

    private static function getServer($appId)
    {
        return OauthCache::findServerByAppid($appId);
    }
}