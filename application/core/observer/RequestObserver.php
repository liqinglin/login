<?php
/**
 */
namespace app\core\observer;
use think\Request;
use think\Validate;
use app\core\error\RequestError;

class RequestObserver implements \SplObserver
{
    public function update(\SplSubject $subject)
    {
        $validate = new Validate();
        $action = strtolower(Request::instance()->action());
        if(isset($subject->validate[$action]['rule'])) {
            $validate->rule($subject->validate[$action]['rule']);

            if(isset($subject->validate[$action]['message'])) {
                $validate->message($subject->validate[$action]['message']);
            }

            $requestType = strtolower(Request::instance()->method());
            $data = Request::instance()->{$requestType}();
            if(!$validate->check($data)) {
                // TODO FIX 需要统一error输出，此处需要处理error
                $error = RequestError::$REQUEST_PARAM_INVALIDATE;
                $errorMessage = $validate->getError();
                if(!empty($errorMessage)) {
                    $error['info'] = $errorMessage;
                }
                $subject->error($error);
            }
        }

        if(isset($subject->validate[$action]['cache'])) {
            Request::instance()->cache('__URL__', $subject->validate[$action]['cache']);
        }
    }
}