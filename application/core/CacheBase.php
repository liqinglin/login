<?php
/**
 * memcache底层类
 * User: honglvshi
 * Date: 2016/11/1
 * Time: 20:26
 */
namespace app\core;
use app\core\exception\MemcacheException;
use think\Cache;
use think\Model;
use think\Config;
use think\exception\HttpException;

class CacheBase
{
    private static $_db = array();
    private static $_cacheName = array();
    /**
     * 获取缓存
     * @param 第一个放key 接下来放参数
     * @return array|bool|mixed|stdClass
     * @throws CacheException
     */
    public static function get($key,$params = false)
    {
        if(empty($key)) {
            throw new MemcacheException('get 方法缺少参数');
        }
        $cacheKey = self::_getCacheKey($key,$params);
        self::_initCacheConfig();
        $info = Cache::get($cacheKey);
        if(empty($info)) {
            return false;
        }
        return json_decode($info,true);
    }

    /*
     * 获取多实例缓存
    */
    private static function _initCacheConfig()
    {
        $list = explode('\\', get_called_class());
        $cur = count($list) - 2;
        if(isset($list[$cur])) {
            $config = Config::get('cache_memcache.' . $list[$cur]);
            if(empty($config)) {
                throw new HttpException(200,'没有找到该配置项:' .'cache_memcache.' . $list[$cur]);
            }
            return Cache::connect($config,$list[$cur]);
        }
        //如果没有找到该配置项
        return false;
    }

    /**
     * 设置缓存
     * @param 第一个放key 最后个放数据 中间放参数
     * @return bool
     * @throws CacheException
     */
    public static function set($key, $info, $params)
    {
        if(!self::checkEmpty($info)) {
            return false;
        }
        $ttlTime = $key['ttl'];
        if(!isset($key['ttl']) || empty($ttlTime)) {
            $ttlTime = 0; //没有设置有效时期 默认为永久
        }
        $cacheKey = self::_getCacheKey($key,$params);
        self::_initCacheConfig();
        return Cache::set($cacheKey,json_encode($info),$ttlTime);
    }

    protected static function checkEmpty($data)
    {
        if($data === '') {
            return false;
        } elseif ($data === false) {
            return false;
        } elseif ($data === null) {
            return false;
        }
        return true;
    }
    /**
     * 清空缓存
     * @param $name
     * @return null
     */
    public static function del($key,$params)
    {
        $info = self::get($key,$params);
        //缓存不存在
        if(empty($info)) {
            return true;
        }
        $cacheKey = self::_getCacheKey($key,$params);
        self::_initCacheConfig();
        return Cache::rm($cacheKey);
    }

    /**
     * 清空关联缓存
     */
    public function delMul()
    {
        if(empty(self::$_db)) {
            return false;
        }
        $className = $this->_getKeyClass();
        $keyObj = new $className();
        $linkArray = $keyObj::getLinkArray();
        if(empty($linkArray)) {
            return true;
        }
        self::_initCacheConfig();
        foreach($linkArray as $configKey) {
            $fields = $keyObj::explain($configKey);
            $fieldsData = $keyObj::fillData(self::$_db,$fields);
            $this->del($configKey,$fieldsData);
        }
        return true;
    }
    /**
     * 更新关联缓存
     * @param $name
     * @return null
     */
    public function save()
    {
        if(empty(self::$_db)) {
            return false;
        }
        $className = $this->_getKeyClass();
        $keyObj = new $className();
        $linkArray = $keyObj::getLinkArray();
        if(empty($linkArray)) {
            return true;
        }
        self::_initCacheConfig();
        foreach($linkArray as $configKey) {
            $fieldsData = $keyObj::fillData(self::$_db, $keyObj::explain($configKey));
            if($fieldsData === false) {
                throw new MemcacheException("缓存key({$configKey['key']})的参数为null");
            }
            $result = $this->set($configKey,self::$_db->getData(),$fieldsData);
            if($result === false) {
                throw new MemcacheException("缓存key({$configKey['key']}) save失败");
            }
        }
        return true;
    }
    public static function init(Model $model)
    {
        self::$_db = $model;
        return new static();
    }
    private function _getKeyClass()
    {
        $class = substr(get_class($this), 0, -5) . 'Keys';
        $str = str_replace('\\model\\','\\key\\',$class);
        return str_replace('\\model\\','\\key\\',$class);
    }
    /**
     * 如果方法不存在抛出异常
     * @param $name
     * @param $arguments
     * @throws CacheException
     */
    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
        throw new MemcacheException("不存在cache调用的方法({$name})");
    }

    /**
     * 缓存key 默认以模块名以前缀 模块-cacheKey
     * @param $key
     * @param $params
     * @return string
     */
    private static function _getCacheKey($key,$params)
    {
        $prefixKey = self::_getPrefix();
        return $prefixKey . '-' . CacheKeyBase::trans($key,$params);
    }

    private static function _getPrefix()
    {
        $className = substr(get_class(new static()), 0, -5);
        $classArray = explode('\\',$className);
        return array_pop($classArray);
    }

}