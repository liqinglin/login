<?php
/**
 * 模型基层
 * User: honglvshi
 * Date: 2016/11/6
 * Time: 16:58
 */
namespace app\core;
use think\Config;
use think\Db;
use think\exception\HttpException;
use think\Model;
class ModelBase extends Model
{

    protected $connection;

    /**
     * 拓展db
     * @param bool $baseQuery
     * @return mixed
     */
    public function db($baseQuery = true)
    {
        $structure = explode('\\',$this->class);
        //去掉模型
        array_pop($structure);
        //获取所属数据库
        $schema = strtolower(array_pop($structure));
        $config = Config::get('database.' . $schema);
        if(empty($config)) {
            throw new HttpException(200,'没有该配置项');
        }
        $this->connection = $config;

        $model = $this->class;
        if (!isset(self::$links[$model])) {
            // 设置当前模型 确保查询返回模型对象
            $query = Db::connect($this->connection)->model($model, $this->query);

            // 设置当前数据表和模型名
            if (!empty($this->table)) {
                $query->setTable($this->table);
            } else {
                $query->name($this->name);
            }

            if (!empty($this->pk)) {
                $query->pk($this->pk);
            }

            self::$links[$model] = $query;
        }
        // 全局作用域
        if ($baseQuery && method_exists($this, 'base')) {
            call_user_func_array([$this, 'base'], [ & self::$links[$model]]);
        }
        // 返回当前模型的数据库查询对象
        return self::$links[$model];
    }
}