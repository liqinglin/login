<?php

/**
 * $Id: SessionError.php 49 2016-12-03 06:05:53Z hls $
 */

namespace app\core\error;

class SessionError extends Error
{
    public static $SESSION_IS_OUT_TIME = array(
        'code'  => 100,
        'info'  => '登录超时,请重新登录',
        'msg'   => 'SESSION_IS_OUT_TIME'
    );


}