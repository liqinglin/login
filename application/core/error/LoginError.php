<?php

/**
 * $Id: ResetpwdError.php 114 2017-03-06 13:31:12Z hls $
 */

namespace app\core\error;

class LoginError extends Error
{
    public static $LOGIN_WRONG_VERIFY_CODE = array(
        'code'  => 100,
        'info'  => '验证码错误',
        'msg'   => 'LOGIN_WRONG_VERIFY_CODE'
    );

    public static $LOGIN_WRONG_USERNAME_OR_PASSWORD = array(
        'code'  => 101,
        'info'  => '用户名或密码错误,请重新输入',
        'msg'   => 'LOGIN_WRONG_USERNAME_OR_PASSWORD'
    );

    public static $LOGIN_WRONG_USER_STATE = array(
        'code'  => 102,
        'info'  => '账户状态异常,请联系官方客服解决',
        'msg'   => 'LOGIN_WRONG_USER_STATE'
    );

    public static $LOGIN_SET_LOGIN_COUNTS_FAIL = array(
        'code'  => 103,
        'info'  => '更新用户登录次数失败',
        'msg'   => 'LOGIN_SET_LOGIN_COUNTS_FAIL'
    );

    public static $LOGIN_WRONG_TWICE_PASSWORD = array(
        'code'  => 104,
        'info'  => '两次密码不一致,请重新输入',
        'msg'   => 'LOGIN_WRONG_TWICE_PASSWORD'
    );

    public static $LOGIN_NO_GET_PWD_UID = array(
        'code'  => 105,
        'info'  => '系统错误,请重试',
        'msg'   => 'LOGIN_NO_GET_PWD_UID'
    );

    public static $LOGIN_RESET_PWD_FAIL = array(
        'code'  => 106,
        'info'  => '重置密码失败',
        'msg'   => 'LOGIN_RESET_PWD_FAIL'
    );

    public static $LOGIN_WRONG_SMS_VCODE = array(
        'code'  => 107,
        'info'  => '手机验证码不正确,请重新输入',
        'msg'   => 'LOGIN_WRONG_SMS_VCODE'
    );

    public static $LOGIN_SMS_VCODE_EXPIRED = array(
        'code'  => 108,
        'info'  => '手机验证码已失效,请重新获取',
        'msg'   => 'LOGIN_SMS_VCODE_EXPIRED'
    );

    public static $LOGIN_WRONG_TRADE_MEMBER_INFO = array(
        'code'  => 109,
        'info'  => '账户信息有误,请重试',
        'msg'   => 'LOGIN_WRONG_TRADE_MEMBER_INFO'
    );

    public static $LOGIN_SMS_CODE_EXCEED_LIMIT = array(
        'code'  => 110,
        'info'  => '对不起，你今日申请验证码次数超限，请联系管理员 ！',
        'msg'   => 'LOGIN_SMS_CODE_EXCEED_LIMIT'
    );

    public static $LOGIN_SMS_CODE_TOO_SHORT_INTERVAL = array(
        'code'  => 111,
        'info'  => '您已经申请过手机验证码，请 %s秒后再试',
        'msg'   => 'LOGIN_SMS_CODE_TOO_SHORT_INTERVAL'
    );

    public static $LOGIN_SMS_VCODE_SEND_FAIL = array(
        'code'  => 112,
        'info'  => '发送验证码失败,请检查您的手机号码',
        'msg'   => 'LOGIN_SMS_VCODE_SEND_FAIL'
    );

    public static $LOGIN_WRONG_LINE_TYPE = array(
        'code'  => 113,
        'info'  => '短信渠道类型错误',
        'msg'   => 'LOGIN_WRONG_LINE_TYPE'
    );

    public static $LOGIN_NOT_AGREED = array(
        'code'  => 114,
        'info'  => '您必须同意商城用户注册协议才可以注册',
        'msg'   => 'LOGIN_NOT_AGREED'
    );

    public static $LOGIN_TRADE_MEMBER_INFO_EMPTY = array(
        'code'  => 115,
        'info'  => '用户信息不存在',
        'msg'   => 'LOGIN_TRADE_MEMBER_INFO_EMPTY'
    );

    public static $LOGIN_TRADE_MEMBER_ADD_FAIL = array(
        'code'  => 116,
        'info'  => '用户信息添加失败',
        'msg'   => 'LOGIN_TRADE_MEMBER_ADD_FAIL'
    );

    public static $LOGIN_TRADE_MEMBER_FUND_ADD_FAIL = array(
        'code'  => 117,
        'info'  => '用户资金信息添加失败',
        'msg'   => 'LOGIN_TRADE_MEMBER_FUND_ADD_FAIL'
    );

    public static $LOGIN_TRADE_MEMBER_FUND_EDIT_FAIL = array(
        'code'  => 118,
        'info'  => '用户资金信息编辑失败',
        'msg'   => 'LOGIN_TRADE_MEMBER_FUND_EDIT_FAIL'
    );

    public static $LOGIN_REGISTER_EXCEPTION = array(
        'code'  => 119,
        'info'  => '用户注册发生异常',
        'msg'   => 'LOGIN_REGISTER_EXCEPTION'
    );

    public static $LOGIN_MOBILE_ALREADY_REGISTERED = array(
        'code'  => 120,
        'info'  => '手机号码已经注册过,请重新注册',
        'msg'   => 'LOGIN_MOBILE_ALREADY_REGISTERED'
    );

    public static $LOGIN_TRADE_MEMBER_INFO_ADD_FAIL = array(
        'code'  => 121,
        'info'  => '用户基本信息表插入失败',
        'msg'   => 'LOGIN_TRADE_MEMBER_INFO_ADD_FAIL'
    );

    public static $LOGIN_TRADE_MEMBER_INFO_EDIT_FAIL = array(
        'code'  => 122,
        'info'  => '用户基本信息表编辑失败',
        'msg'   => 'LOGIN_TRADE_MEMBER_INFO_EDIT_FAIL'
    );

    public static $LOGIN_USERS_CASH_INIT_FAIL = array(
        'code'  => 123,
        'info'  => '零售商城用户基本信息初始化失败',
        'msg'   => 'LOGIN_USERS_CASH_INIT_FAIL'
    );

    public static $LOGIN_TRANSFER_SESSION_ID_FAIL = array(
        'code'  => 124,
        'info'  => '跳转到批发商城失败',
        'msg'   => 'LOGIN_TRANSFER_SESSION_ID_FAIL'
    );
}