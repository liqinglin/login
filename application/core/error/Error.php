<?php

/**
 * 错误码使用7位数字，1、2位为项目代码，3、4为模块代码，最后3位为具体错误码
 *
 * 10 10 100
 * 项目 模块 具体错误码
 *
 * @author GreatWall
 *
 */
namespace app\core\error;
use think\exception\HttpException;
class Error
{
    const UNKNOWN_ERROR = 9999999;
    const APP_ID = 11;

    public static $ERROR_MODULE_MAP = array(
        'OAUTH'     => 10,
        'REQUEST'   => 11,
        'SESSION'   => 12,
        'RESETPWD'  => 13,
        'LOGIN'     => 14,
    );

    public static function trans($error, $params = array())
    {
        $msg = isset($error['msg']) ? $error['msg'] : 'UNKNOW_ERROR_MESSAGE';
        list($model, $enMsg) = explode('_', $msg, 2);
        $code = self::APP_ID . self::$ERROR_MODULE_MAP[$model] . $error['code'];
        $data = empty($error['data']) ? new \stdClass() : (object)$error['data'];
        $info = $error['info'];
        if(!empty($params)) {
            $info = vsprintf($error['info'], $params);
        }
        return array('code' => strval($code), 'msg' => strval($enMsg), 'info' => strval($info), 'data' => $data);
    }

    public static function send($error, $params)
    {
        $error = self::trans($error, $params);
        $error['status'] = '0';
        echo json_encode($error,JSON_UNESCAPED_UNICODE);
        exit();
    }

    public static function explain($error, $params)
    {
        if(!isset($error['info'])){
            throw new HttpException(200,'Error::explain need info');
        }
        if(empty($params)) {
            throw new HttpException(200,'Error::explain need params');
        }
        $error['info'] = vsprintf($error['info'], $params);
        return $error;
    }
}