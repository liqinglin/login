<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/8
 * Time: 11:58
 */
namespace app\core\error;
class OauthError extends Error
{
    public static $OAUTH_TOKEN_EXPIRED = array(
        'code'  => 100,
        'info'  => '授权缓存过期，请重新授权',
        'msg'   => 'OAUTH_TOKEN_EXPIRED'
    );

    public static $OAUTH_CAPTCHA_IS_EMPTY = array(
        'code'  => 101,
        'info'  => '请输入验证码',
        'msg'   => 'OAUTH_CAPTCHA_IS_EMPTY'
    );

    public static $OAUTH_CAPTCHA_IS_ERROR = array(
        'code'  => 102,
        'info'  => '验证码错误',
        'msg'   => 'OAUTH_CAPTCHA_IS_ERROR'
    );

    public static $OAUTH_REGISTER_ERROR = array(
        'code'  => 102,
        'info'  => '',
        'msg'   => 'OAUTH_REGISTER_ERROR'
    );

    public static $OAUTH_LOGIN_ERROR = array(
        'code'  => 103,
        'info'  => '',
        'msg'   => 'OAUTH_LOGIN_ERROR'
    );

    public static $OAUTH_SERVER_IS_NOT_FIND = array(
        'code'  => 104,
        'info'  => '服务平台查询不到',
        'msg'   => 'OAUTH_SERVER_IS_NOT_FIND'
    );

    public static $OAUTH_IP_IS_NOT_ALLOWED= array(
        'code'  => 105,
        'info'  => 'IP(%s)被限制',
        'msg'   => 'OAUTH_IP_IS_NOT_ALLOWED'
    );
}