<?php

/**
 * $Id: ResetpwdError.php 114 2017-03-06 13:31:12Z hls $
 */

namespace app\core\error;

class ResetpwdError extends Error
{
    public static $GET_CHAIN_RESETPWD_INFO_ERROR = array(
        'code'  => 100,
        'info'  => '错误信息',
        'msg'   => 'RESETPWD_GET_CHAIN_RESETPWD_INFO_ERROR'
    );

    public static $CHAILD_ACCOUNT_IS_DENY_TO_FIND_PASSWORD = array(
        'code'  => 101,
        'info'  => '子账号不允许找回密码',
        'msg'   => 'RESETPWD_CHAILD_ACCOUNT_IS_DENY_TO_FIND_PASSWORD'
    );

    public static $CHAIN_USERNAME_IS_EMPTY = array(
        'code'  => 102,
        'info'  => '账号为空,请重新验证',
        'msg'   => 'RESETPWD_CHAIN_USERNAME_IS_EMPTY'
    );

    public static $CHAIN_TELEPHONE_NOT_BIND = array(
        'code'  => 103,
        'info'  => '用户没有绑定手机号',
        'msg'   => 'RESETPWD_CHAIN_TELEPHONE_NOT_BIND'
    );

    public static $CHAIN_EMAIL_NOT_BIND = array(
        'code'  => 104,
        'info'  => '用户没有绑定邮箱号',
        'msg'   => 'RESETPWD_CHAIN_TELEPHONE_NOT_BIND'
    );

    public static $CHAIN_RESET_PWD_IS_NOT_VALIDATE= array(
        'code'  => 105,
        'info'  => '修改密码失败,请通过校验',
        'msg'   => 'RESETPWD_CHAIN_RESET_PWD_IS_NOT_VALIDATE'
    );

    public static $CHAIN_RESET_PWD_CODE_ERROR = array(
        'code'  => 106,
        'info'  => '修改密码失败,验证码错误',
        'msg'   => 'RESETPWD_CHAIN_RESET_PWD_CODE_ERROR'
    );

    public static $CHAIN_VERITY_RETURN_ERROR = array(
        'code'  => 107,
        'info'  => '修改密码失败,验证码错误',
        'msg'   => 'RESETPWD_CHAIN_VERITY_RETURN_ERROR'
    );

    public static $CHAIN_SEND_CODE_RETURN_ERROR = array(
        'code'  => 108,
        'info'  => '修改密码失败,验证码错误',
        'msg'   => 'RESETPWD_CHAIN_SEND_CODE_RETURN_ERROR'
    );

    public static $USER_USERNAME_IS_NOT_FOUNT = array(
        'code'  => 109,
        'info'  => '用户账号不存在',
        'msg'   => 'RESETPWD_USER_USERNAME_IS_NOT_FOUNT'
    );

    public static $USER_USERNAME_IS_EMPTY = array(
        'code'  => 110,
        'info'  => '账号为空,请重新验证',
        'msg'   => 'RESETPWD_USER_USERNAME_IS_EMPTY'
    );

    public static $USER_VERITY_RETURN_ERROR = array(
        'code'  => 111,
        'info'  => '修改密码失败,验证码错误',
        'msg'   => 'RESETPWD_USER_VERITY_RETURN_ERROR'
    );

    public static $USER_RESET_PWD_IS_NOT_VALIDATE= array(
        'code'  => 112,
        'info'  => '修改密码失败,请通过校验',
        'msg'   => 'RESETPWD_USER_RESET_PWD_IS_NOT_VALIDATE'
    );

    public static $RESETPWD_SSO_DOMAIN_RETURN_ERROR= array(
        'code'  => 113,
        'info'  => '',
        'msg'   => 'RESETPWD_SSO_DOMAIN_RETURN_ERROR'
    );
}