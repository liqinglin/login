<?php

/**
 * $Id: RequestError.php 86 2017-02-20 09:04:39Z sw $
 */

namespace app\core\error;

class RequestError extends Error
{
    public static $REQUEST_IS_EXCEPTION = array(
        'code'  => 100,
        'info'  => '请求通信返回异常 : %s',
        'msg'   => 'REQUEST_IS_EXCEPTION'
    );
    public static $REQUEST_PARAM_INVALIDATE = array(
        'code'  => 101,
        'info'  => '参数错误',
        'msg'   => 'REQUEST_PARAM_INVALIDATE'
    );
    public static $REQUEST_EMPTY_RESPONSE = array(
        'code'  => 102,
        'info'  => '服务器空白响应',
        'msg'   => 'REQUEST_EMPTY_RESPONSE'
    );
    public static $REQUEST_DOMAIN_IS_ILLEGAL = array(
        'code'  => 103,
        'info'  => '非法地址请求',
        'msg'   => 'REQUEST_DOMAIN_IS_ILLEGAL'
    );
    public static $REQUEST_API_ERROR = array(
        'code'  => 104,
        'info'  => '请求api接口返回错误',
        'msg'   => 'REQUEST_API_ERROR'
    );
}