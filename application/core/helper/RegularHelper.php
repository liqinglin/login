<?php
/**
 * 正则帮助类
 * User: honglvshi
 * Date: 2016/11/15
 * Time: 9:45
 */
namespace app\core\helper;
class RegularHelper
{
    private static $_simplePasswordArray = [
        '123456',
        '1234567',
        '12345678',
        '123456789',
        '456789',
        '456123',
        '789456',
        '0123456',
        '012345',
        '01234567',
        '012345678',
        '0123456789'
    ];

    /**
     * 验证连锁账号密码
     * @param $password
     */
    public static function checkChainPassword($password)
    {
        $password = trim($password);
        $passwordLength = mb_strlen($password,'utf8');
        //1.判断是不是9位以下纯数字
        if(self::checkNormalReg('number',$password)) {
            if($passwordLength < 9) {
                return false;
            }
        }
        //2.判断长度是不是在6~16字符
        if($passwordLength < 6 || $passwordLength > 16) {
            return false;
        }
        return true;
    }

    /*
     * 验证密码
     */
    public static function checkPassword($password)
    {
        $password = trim($password);
        //1.判断是不是在简单密码里面
        if(in_array($password,self::$_simplePasswordArray)) {
            return false;
        }
        //2.判断是不是纯数字
        if(self::checkNormalReg('number',$password)) {
            return false;
        }
        //3.判断是不是纯字母
        if(self::checkNormalReg('english',$password)) {
            return false;
        }
        //4.判断长度是不是在6~16字符
        $passwordLength = mb_strlen($password,'utf8');
        if($passwordLength < 6 || $passwordLength > 16) {
            return false;
        }
        return true;
    }
    public static function checkTime($value)
    {
        list($hour, $min) = explode(":", $value);
        if($hour >= 0 && $hour <= 23 && $min >= 0 && $min <= 59) {
            return true;
        }
        return false;
    }
    public static function checkNormalReg($rule, $value)
    {
        $zh = '\x{4e00}-\x{9fa5}';
        $other = '~`!@#$%\^&*()_\-+=\[\]{}|\\:;"\',?.\/><\\\\';
        $validate = array(
            'require' => '/.+/',
            'email' => '/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/',
            'url' => '/^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/',
            'currency' => '/^\d+(\.\d+)?$/',
            'number' => '/^\d+$/',
            'zip' => '/^[1-9]\d{5}$/',
            'integer' => '/^[-\+]?\d+$/',
            'double' => '/^[-\+]?\d+(\.\d+)?$/',
            'english' => '/^[A-Za-z]+$/',
            'zh' => '/^[' . $zh . ']+$/u',
            'en_num' => '/^[A-Za-z0-9]+$/',
            'en_num_zh' => '/^[A-Za-z0-9' . $zh . ']+$/u',
            'en_zh' => '/^[A-Za-z' . $zh . ']+$/u',
            'ip' => '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/',
            'telephone' => '/^1\d{10}$/',
            'date' => '/^\d{4}-\d{2}-\d{2}$/',
            'datetime' => '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/',
            'other' => '/^[' . $other . ']+$/',
            'en_num_other' => '/^[A-Za-z0-9' . $other . ']+$/',
            'en_num_zh_other' => '/^[A-Za-z0-9' . $zh . '' . $other . ']+$/u'
        );
        if(isset($validate[strtolower($rule)])) {
            $rule2 = $validate[strtolower($rule)];
        }
        $result = 1 === preg_match($rule2, $value);
        if($result) {
            return true;
        }
        return false;
    }
}