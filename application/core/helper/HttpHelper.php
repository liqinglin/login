<?php
/**
 * Http通信协议
 * User: honglvshi
 * Date: 2016/11/8
 * Time: 9:43
 */
namespace app\core\helper;
use think\exception\HttpException;

class HttpHelper
{
    const STRING_SECRET_CODE = 'sb2Sjs8n2KwZZweHqq1e4NAD2XCwHwM5_';

    private static $allowRequestType = [
        'get',
        'post',
        'delete',
        'put'
    ];
    public static $status;

    private static $headers = array();
    private static $curl;

    public static function doRequest($url,$data,$method = 'get')
    {
        $method = strtolower($method);
        if(!in_array($method,self::$allowRequestType)) {
            throw new HttpException(200,"不合法的请求类型method:{$method}");
        }
        $info = self::$method($url,$data);
        return $info;
    }

    /**
     * 组装url
     */
    public static function assembleUrl($url,$param)
    {
        if(is_array($param)) {
            $param = http_build_query($param);
        }
        if(stripos($url,'?') > 0) {
            return $url . '&' . $param;
        }
        return $url . '?' . $param;
    }

    /**
     * 初始化
     */
    private static function init($url){
        self::$curl = curl_init();
        curl_setopt(self::$curl, CURLOPT_URL, $url);
        curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, 1);
        return true;
    }

    public static function get($url,$data) {
        $url = self::assembleUrl($url,$data);
        self::init($url);
        curl_setopt(self::$curl, CURLOPT_HTTPGET, TRUE);
        $info = curl_exec(self::$curl);
        curl_close(self::$curl);
        self::$curl = null;
        return $info;
    }

    public static function post($url,$data = false) {
        self::init($url);
        curl_setopt(self::$curl, CURLOPT_POST, 1);
        curl_setopt(self::$curl, CURLOPT_POSTFIELDS, $data);
        $info = curl_exec(self::$curl);
        curl_close(self::$curl);
        self::$curl = null;
        return $info;
    }

    public static function put($url,$data) {
        self::init($url);
        curl_setopt(self::$curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt(self::$curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $info = curl_exec(self::$curl);
        curl_close(self::$curl);
        self::$curl = null;
        return $info;
    }

    public static function delete($url,$data = false) {
        self::init($url);
        curl_setopt(self::$curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt(self::$curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $info = curl_exec(self::$curl);
        curl_close(self::$curl);
        self::$curl = null;
        return $info;
    }

    public static function httpPost($data,$url)
    {
        $v = [
            'v' => self::dataEncode($data)
        ];
        $returnInfo = self::doRequest($url, $v,'post');
        if(empty($returnInfo)){
            return [
                'status' => 0,
                'info'    => '数据错误',
            ];
        }
        $info = json_decode($returnInfo,true);
        if(empty($info) || !is_array($info)){
            return [
                'status' => 0,
                'info'    => '解析错误',
            ];
        }
        return $info;
    }

    private static function dataEncode($data)
    {
        if(empty($data)){
            return '';
        }
        $data = json_encode($data,JSON_UNESCAPED_UNICODE);
        return base64_encode(self::STRING_SECRET_CODE.$data);
    }

    public static function dataDecode($data)
    {
        if(empty($data)){
            return '';
        }
        $data = base64_decode($data);
        $str  = str_replace(self::STRING_SECRET_CODE,'',$data);
        return json_decode($str,true);
    }

}