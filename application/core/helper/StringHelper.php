<?php
/**
 * 字符串帮助类
 * $Id$
 */
namespace app\core\helper;

class StringHelper
{
    const STRING_SECRET_CODE = 'sb2Sjs8n2KwZZweHqq1e4NAD2XCwHwM5';

    /**
     * 系统非常规MD5加密方法
     * @param  string $str 要加密的字符串
     * @return string
     */
    public static function thinkUcenterMd5($str, $key = 'ThinkUCenter'){
        return '' === $str ? '' : md5(sha1($str) . $key);
    }

    /**
     * 根据密钥加密的方式
     */
    public static function stringEncode($str)
    {
        return base64_encode(self::STRING_SECRET_CODE.'_'.$str);
    }
}