<?php
/**
 * 签名帮助类
 * User: honglvshi
 * Date: 2016/11/3
 * Time: 19:26
 */
namespace app\core\helper;

class IpHelper
{
    public static function checkAccessIp($ip,$allowedIPs)
    {
        if(!is_array($allowedIPs)) {
            return true;
        }
        foreach ($allowedIPs as $filter) {
            if (empty($filter) || $filter === '*' || $filter === $ip || (($pos = strpos($filter, '*')) !== false && !strncmp($ip, $filter, $pos))) {
                return true;
            }
        }
        return false;
    }
}