<?php
/**
 * 签名帮助类
 * User: honglvshi
 * Date: 2016/11/3
 * Time: 19:26
 */
namespace app\core\helper;
use app\core\exception\SignException;

class SignHelper
{
    public static function makeSha1String($param,$oauthSecret)
    {
        if(!is_array($param)) {
            throw new SignException('no param to sign');
        }
        if(empty($oauthSecret)) {
            throw new SignException('no oauthSecret to sign');
        }
        $baseString = self::getBaseString($param,$oauthSecret);
        return sha1($baseString);
    }

    public static function getBaseString($param,$oauthSecret)
    {
        ksort($param);
        $baseString = self::ToUrlParams($param);
        $baseString .= '&oauth_secret=' . $oauthSecret;
        $baseString = strtolower($baseString);
        return $baseString;
    }

    /**
     * 格式化参数格式化成url参数
     */
    public static function ToUrlParams($data)
    {
        $buff = "";
        foreach ($data as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= trim($k) . "=" . trim($v) . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 创建用户信息签名
     */
    public static function createUserInfoSign($uid,$username,$utype)
    {
        $str = '-z'.$uid.'+-*/'.$username.'_'.$utype;
        return md5($str);
    }
}