<?php
/**
 * 发送手机短信
 * $Id$
 */
namespace app\core\helper;

use think\Config;
class SmsHelper
{
    public static function sendSms($mobile,$content,$lineType='JNB')
    {
        $content    = str_replace('%','%25',$content);
        $smsConfig  = self::getSmsConfig($lineType);
        $url        = "http://202.91.244.252/qd/SMSSendYD";
        $content    = iconv('UTF-8','GB2312',$content);
        $postData   = "usr=".$smsConfig['USERNAME']."&pwd=".$smsConfig['PASSWORD']."&mobile=".$mobile."&sms=".urlencode($content)."&extdsrcid=";
        $returnInfo = HttpHelper::doRequest($url, $postData,'post');
        $info = explode(",",$returnInfo);
        if(empty($info) || 2 != count($info) || $info[0] < 0){
        	return [
                'info'   => $info,
                'status' => 0,  //失败
            ];
        }
        return [
            'info'   => $info,
            'status' => 1,   //成功
        ];
    }

    private static function getSmsConfig($type = ''){
        if(empty($type)){
            return Config::get('SMS_CONFIG');
        }else{
            return Config::get('SMS_CONFIG_'.$type);
        }
    }

}