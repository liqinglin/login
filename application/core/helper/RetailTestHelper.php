<?php
/**
 * 与零售商城项目的交互
 * $Id$
 */
namespace app\core\helper;

use think\Config;
use app\core\error\LoginError;
class RetailTestHelper
{
    /**
     * 初始化零售商城用户信息
     */
    public static function initUsersCash($uid)
    {
        $data = [
	        'uid' => $uid,
        ];
        $url        = Config::get('RETAIL_URL').'Home/LoginApi/logininit';
        $returnInfo = HttpHelper::doRequest($url, $data,'post');
        $info       = json_decode($returnInfo,true);
        if(empty($returnInfo) || 0 == $info['status']){
        	return LoginError::$LOGIN_USERS_CASH_INIT_FAIL;
        }
        return true;
    }
}