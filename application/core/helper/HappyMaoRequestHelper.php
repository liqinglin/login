<?php
/**
 * 与批发商城项目的交互
 * $Id$
 */
namespace app\core\helper;

use think\Config;
class HappyMaoRequestHelper
{
    /**
     * 传递session_id到批发商城首页
     */
    public static function transferSessionId($sessionId)
    {
        $data = [
	        'session_id' => $sessionId,
        ];
        $url = Config::get('HAPPY_MAO_URL').'api.php?s=default/api_login';
        return HttpHelper::httpPost($data,$url);
    }

}