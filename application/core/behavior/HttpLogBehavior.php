<?php

/**
 * 网络通讯抓包
 * $Id: HttpLogBehavior.php 1814 2017-03-17 06:11:39Z myc $
 */
namespace app\core\behavior;

use think\Request;
use app\core\CoobarLog;

class HttpLogBehavior
{
    public function run(&$params)
    {
        $request = Request::instance();
        $method = strtolower($request->method());
        $baseUrl = $request->baseUrl();

        $httpCap = $baseUrl;

        $getData = $request->get();
        if(!empty($getData)){
            $httpCap .= '[GET]'.json_encode($getData,JSON_UNESCAPED_UNICODE);
        }
        if('get' == $method){
        }else if('put' == $method){
            $httpCap .= '[PUT]'.json_encode($request->put(),JSON_UNESCAPED_UNICODE);
        }else if('post' == $method){
            $httpCap .= '[POST]'.json_encode($request->post(),JSON_UNESCAPED_UNICODE);
        }else if('delete' == $method){
            $httpCap .= '[DELETE]'.json_encode($request->delete(),JSON_UNESCAPED_UNICODE);
        }

        $data = $params->getContent();
        $data = $params->jsonArrayValueToString($data);
        $httpCap .= '[RESPONSE]'.$data;
        CoobarLog::info($httpCap,'http');
    }
}