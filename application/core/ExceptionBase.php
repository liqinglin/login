<?php
namespace app\core;
use think\Config;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\Response;

class ExceptionBase extends Handle
{

    public function render(\Exception $e)
    {
        if ($e instanceof HttpException) {
            $statusCode = $e->getStatusCode();
        }
        //TODO::开发者对异常的操作
        $error['status'] = '0';
        $error['msg'] = $e->getMessage();
        $error['type'] = 'exception';
        $error['code'] = '9999999';
        return json($error);
        $response = Response::create($error, Config::get('default_return_type'), 200, [], []);
        throw new HttpResponseException($response);
        //可以在此交由系统处理
        return parent::render($e);
    }

}