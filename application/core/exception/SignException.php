<?php

/**
 * $Id: SignException.php 26 2016-11-25 11:28:43Z myc $
 */
namespace app\core\exception;

use think\exception\HttpException;

class SignException extends HttpException
{
    public function __construct($msg, $code = 20)
    {
        parent::__construct($code,$msg);
    }
}