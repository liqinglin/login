<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/11/1
 * Time: 10:56
 */
namespace app\core\exception;

use think\exception\HttpException;

class MemcacheException extends HttpException
{
   public function __construct($msg, $code = 20)
   {
       parent::__construct($code,$msg);
   }
}