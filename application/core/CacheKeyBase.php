<?php
/**
 * Cache底层key基类
 * User: honglvshi
 * Date: 2016/11/1
 * Time: 20:44
 */
namespace app\core;
use app\core\exception\MemcacheException;
use think\Model;

class CacheKeyBase
{

    /**
     * 代理填充key
     * @param $key
     * @param $data
     */
    public static function trans($keys,$parms = false)
    {
        if(empty($keys) || !isset($keys['key']) || empty($keys['key'])) {
            throw new MemcacheException('no trans key');
        }
        $keyCnt = substr_count($keys['key'],'%');
        //缓存key不包含% 则默认认为不需要被填充
        if($keyCnt === 0 && $parms === false) {
            return $keys['key'];
        }
        $parmsCnt = count($parms);
        if($parmsCnt < $keyCnt) {
            throw new MemcacheException('传入参数小于字符串的参数');
        }
        $cacheKey = vsprintf($keys['key'], $parms);
        if(empty($cacheKey)) {
            throw new MemcacheException('key解析失败');
        }
        return $cacheKey;
    }

    /**
     * 解析key
     * @param $key
     */
    public static function explain($keys)
    {
        $keyCnt = substr_count($keys['key'],'%');
        if($keyCnt == 0) {
            return $keys['key'];
        }
        $compositeArray = explode('-',$keys['key']);
        $field = array();
        if(count($compositeArray) == 1) {
            $key = $compositeArray[0];
            $params = explode('.',$key);
            $field[] = $params[0];
            return $field;
        }
        foreach($compositeArray as $key) {
            $params = explode('.', $key);
            $field[] = $params[0];
        }

        if(empty($field) || !is_array($field)) {
            throw new MemcacheException('key格式不对 解析失败');
        }
        return (array)$field;
    }

    /**
     * 根据model填充数据
     */
    public static function fillData(Model $model,$fields)
    {
        foreach($fields as $key => $value) {
            $fields[$key] = $model->{$value};
            if(is_null($fields[$key])) {
                return false;
            }
        }
        return $fields;
    }

    /**
     * 获取联合key
     * @param $key
     */
    public static function getLinkArray()
    {
    }



}