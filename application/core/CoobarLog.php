<?php

/**
 * 日志输出管理
 * $Id: CoobarLog.php 1657 2017-03-09 10:00:37Z hls $
 */
namespace app\core;

use think\Config;
use think\Request;

class CoobarLog
{
    /**
     * <p>严重错误: 导致系统崩溃无法使用</p>
     * <p>警戒性错误: 必须被立即修改的错误</p>
     * <p>临界值错误: 超过临界值的错误，例如一天24小时，而输入的是25小时这样</p>
     * <p>一般错误: 一般性错误，但不影响程序运行</p>
     *
     * <i>存储共享池(正式服)+邮件通知</i>
     * @var ERR
     */
    const ERR     = 'ERR';

    /**
     * <p>警告性错误: 代码有容错控制，但是已经会影响功能正常使用，会出现潜在的未知问题</p>
     * <i>存储本地机器(正式服)</i>
     * @var WARN
     */
    const WARN    = 'WARN';

    /**
     * <p>通知: 程序可以运行但是还不够完美的错误</p>
     * <i>存储本地机器(开发服|测试服)</i>
     * @var NOTICE
     */
    const NOTICE  = 'NOTIC';

    /**
     * <p>信息: 重要业务级别，强调应用程序的运行全程，粗粒度级别</p>
     * <i>存储本地机器(开发服|测试服|正式服)</i>
     * @var INFO
     */
    const INFO    = 'INFO';

    /**
     * <p>调试: 方便日志过滤和观察，细粒度级别</p>
     * <i>存储本地机器(开发服|测试服)</i>
     * @var DEBUG
     */
    const DEBUG   = 'DEBUG';

    public static function error($message,$module=''){
        return self::_write($message,$module,self::ERR,'');
    }

    public static function warn($message,$module=''){
        return self::_write($message,$module,self::WARN,'');
    }

    public static function notice($message,$module=''){
        return self::_write($message,$module,self::NOTICE,'');
    }

    public static function info($message,$module=''){
        return self::_write($message,$module,self::INFO,'');
    }

    public static function debug($message,$module=''){
        return self::_write($message,$module,self::DEBUG,'');
    }

    private static function _write($message,$module='',$level=self::NOTICE,$file='') {
        if(is_array($message) || is_object($message)) {
            $message = print_r($message,true);
        }
        $logPath = self::_getLogPath($module);
        if(false === $logPath){
            return false;
        }
        if('' == $file){
            $file = date('H').'.log';
        }
        $destination = $logPath.date('Ymd').'/'.$file;
        $path = dirname($destination);
        !is_dir($path) && mkdir($path, 0755, true);

        $ip = Request::instance()->ip();

    	return error_log('['.date("Y-m-d H:i:s").']['.$ip.']['.$level.']'.$message."\n", 3,$destination);
    }

    private static function _getLogPath($module){
        $config = Config::get('coobar_log');
        $logPath = '';
        if('' != $module && isset($config[$module])){
            $logPath = $config[$module];
        }
        if('' == $logPath){
            if(empty($config['default'])){
                return false;
            }
            $logPath = $config['default'];
        }
        return $logPath;
    }
}