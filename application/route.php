<?php
use think\Route;

//登录账号
Route::controller('login','index/Login');
//账号密码
Route::controller('password','index/Password');
//公共接口
Route::controller('pub','index/Pub');
//注册接口
Route::controller('register','index/Register');
//零售商城api接口
Route::controller('retailapi','index/RetailApi');