<?php
/**
 * $Id$
 */
namespace app\index\controller;
use app\core\Base;
use think\Request;
use app\index\facade\LoginFacade;

class Login extends Base
{
    public $validate = [
        'postlogin' => [
            'rule' => [
                'username|用户名' => 'require',
                'password|密码'  => 'require|length:6,30',
                'vcode|验证码'    => 'require',
            ],
        ],
    ];

    /**
     * 登录
     */
    public function postLogin()
    {
        $request = Request::instance()->post();
        $sessionInfo  = LoginFacade::doLogin($request['username'],$request['password'],$request['vcode']);
        if(isset($sessionInfo['code'])){
            $this->error($sessionInfo);
        }
        $this->success($sessionInfo);
    }
}