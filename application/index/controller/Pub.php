<?php

/**
 * $Id: Pub.php 2283 2017-04-14 02:31:53Z hls $
 */

namespace app\index\controller;

use app\core\Base;
use think\captcha\Captcha;
use think\Request;
use app\index\facade\VcodeFacade;

class Pub extends Base
{
    public $validate = [
        'getcaptcha' => [
            'rule' => [
            ],
        ],
        'postsmsvcode' => [
            'rule' => [
                'vcode|验证码'           => 'require',
                'mobile|手机号'          => 'require|telephone',
                'skip_vcode|是否忽略验证码' => 'in:0,1',  //0：不忽略，要验证 1:忽略，不验证
                'line_type|短信渠道类型'   => 'require',
            ],
        ],
    ];

    /**
     * 获取验证码
     */
    public function getCaptcha()
    {
        $captcha = new Captcha();
        return $captcha->entry();
    }

    /**
     * 发送短信验证码
     */
    public function postSmsVcode()
    {
        $request = Request::instance()->post();
        $request['skip_vcode'] = Request::instance()->post('skip_vcode',0);
        $request['line_type']  = Request::instance()->post('line_type','');
        $result  = VcodeFacade::sendSmsVcode($request['vcode'],$request['mobile'],$request['skip_vcode'],$request['line_type']);
        if(isset($result['code'])){
        	$this->error($result);
        }

        $this->success();
    }

}