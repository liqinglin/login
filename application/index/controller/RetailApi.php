<?php
/**
 * 供零售商城调用的api接口
 * $Id$
 */
namespace app\index\controller;
use app\core\Base;
use think\Request;
use app\index\facade\PasswordFacade;

class RetailApi extends Base
{
    public $validate = [
        'postcheckuseridandpassword' => [
            'rule' => [
                'user_id|用户id'   => 'require',
                'password|登录密码' => 'require|length:6,30',
            ],
        ],
    ];

    /**
     * 检查用户名和密码
     */
    public function postCheckUserIdAndPassword()
    {
        $request = Request::instance()->post();
        $userInfo = PasswordFacade::getUserInfoByUserIdAndPassword($request['user_id'],$request['password']);
        if(isset($userInfo['code'])){
            $this->error($userInfo);
        }

        $this->success($userInfo);
    }
}