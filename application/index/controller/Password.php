<?php
/**
 * $Id$
 */
namespace app\index\controller;
use app\core\Base;
use think\Request;
use app\index\facade\PasswordFacade;

class Password extends Base
{
    public $validate = [
        'getcheckuserinfo' => [
            'rule' => [
                'mobile|手机号'      => 'require|telephone',
                'uid|商户账号'        => 'require',
                'sms_vcode|手机验证码' => 'require|length:6',
            ],
        ],
        'putpassword' => [
            'rule' => [
                'password|登录密码'     => 'require|length:6,30',
                'cfm_password|确认密码' => 'require|length:6,30',
            ],
        ],
    ];

    /**
     * 找回密码时检查商户信息
     */
    public function getCheckUserInfo()
    {
        $request = Request::instance()->get();
        $result = PasswordFacade::checkUserInfo($request['mobile'],$request['uid'],$request['sms_vcode']);
        if(isset($result['code'])){
            $this->error($result);
        }
        $this->success();
    }

    /**
     * 商户信息检查通过后重置密码
     */
    public function putPassword()
    {
        $request = Request::instance()->put();
        $result  = PasswordFacade::resetPwd($request['password'],$request['cfm_password']);
        if(isset($result['code'])){
            $this->error($result);
        }
        $this->success();
    }
}