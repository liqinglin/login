<?php
/**
 * $Id$
 */
namespace app\index\controller;
use app\core\Base;
use think\Request;
use app\index\facade\RegisterTestFacade;

class RegisterTest extends Base
{
    public $validate = [
        'postregister' => [
            'rule' => [
                'mobile|手机号'        => 'require|telephone',
                'sms_vcode|手机验证码'  => 'require',
                'is_agreed|是否同意协议' => 'require|in:0,1',
                'password|登录密码'     => 'require|length:6,30',
                'cfm_password|确认密码' => 'require|length:6,30',
                'track_uid',
            ],
        ],
    ];

    /**
     * 注册用户
     */
    public function postRegister()
    {
        $request   = Request::instance()->post();
        $request['track_uid'] = Request::instance()->post('track_uid',0);
        $sessionInfo = RegisterTestFacade::doRegister($request);
        if(isset($sessionInfo['code'])){
            $this->error($sessionInfo);
        }
        $this->success($sessionInfo);
    }
}