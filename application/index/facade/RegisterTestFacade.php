<?php

/**
 * $Id: RequestHandleFacade.php 29 2016-11-26 05:53:55Z myc $
 */
namespace app\index\facade;
use app\core\Facade;
use app\core\error\LoginError;
use app\core\helper\VcodeHelper;
use think\Config;
use app\data\dbcache\model\prime\TradeMemberCache;
use app\data\dbcache\model\prime\TradeAgentCache;
use app\data\dbcache\model\prime\TradeMemberFundCache;
use think\Request;
use app\core\helper\StringHelper;
use app\data\dbcache\model\prime\TradeMemberInfoCache;
use app\core\helper\RetailHelper;
use app\core\helper\RetailTestHelper;

class RegisterTestFacade extends Facade
{
    /**
     * 注册用户
     */
    public static function doRegister($request)
    {
        if(0 == $request['is_agreed']){
            return LoginError::$LOGIN_NOT_AGREED;
        }
        if($request['password'] != $request['cfm_password']){
        	return LoginError::$LOGIN_WRONG_TWICE_PASSWORD;
        }
        $mobileInfo = TradeMemberCache::getTradeMemberInfoByMobile($request['mobile']);
        if(!empty($mobileInfo)){
//         	return LoginError::$LOGIN_MOBILE_ALREADY_REGISTERED;
        }
        $result = VcodeHelper::checkSmsVcode($request['mobile'],$request['sms_vcode']);
        if(isset($result['code'])){
            return $result;
        }
        try{
            $tradeMemberObj = TradeMemberCache::getTradeMemberModel();
            $tradeMemberObj->startTrans();
            $trackUid = $request['track_uid'];
            $devPath  = '';
            $agentid  = '';
            if($trackUid > 0){  //被推广来注册
                $userInfo = TradeMemberCache::getTradeMemberInfoByUid($trackUid);
                if(!empty($userInfo) && $userInfo['uid'] > 0){  //存在该推荐人用户
                    $agentInfo = TradeAgentCache::getTradeAgentInfoByAgentTradeUid($trackUid);
                    if(!empty($agentInfo)){  //如果推荐人是代理商,则客户归属于该代理商, o_trade_member.agentid = o_trade_agent.agent_code
                        $agentid = $agentInfo['agent_code'];
                    }else{
                        $agentid = $userInfo['agentid'];   //推荐人不是代理商,则绑定该推荐人所归属的代理商编码
                    }
                    if ($userInfo['dev_uid'] > 0) {  //该推荐人注册时也是被其他人推荐
                        $devPath = "{$trackUid}/{$userInfo['dev_uid']}";
                    }
                    $result = TradeMemberInfoCache::incrConsumerCouponAmountByUid(100, $userInfo['uid']);  //给推荐人累计商城消费券
                    if(false === $result){
                        $tradeMemberObj->rollback();
                        return LoginError::$LOGIN_TRADE_MEMBER_INFO_EDIT_FAIL;
                    }
                }
            }
            $insertData = self::insertTradeMember($agentid,$request['mobile'],$request['password'],$trackUid,$devPath);
            if(isset($insertData['code'])){
                $tradeMemberObj->rollback();
                return $insertData;
            }
            $uid = $insertData['uid'];
            $result = self::insertTradeMemberFund($uid);
            if(isset($result['code'])){
                $tradeMemberObj->rollback();
                return $result;
            }
            $result = self::insertTradeMemberInfo($uid,$trackUid);  //插入批发商城用户基本信息
            if(isset($result['code'])){
                $tradeMemberObj->rollback();
                return $result;
            }
            $result = RetailTestHelper::initUsersCash($uid);  //插入零售商城用户基本信息
            if(isset($result['code'])){
                $tradeMemberObj->rollback();
                return $result;
            }

            $tradeMemberObj->commit();
        }catch(\Exception $e){
            $tradeMemberObj->rollback();
            $error = LoginError::$LOGIN_REGISTER_EXCEPTION;
            $error['info'] = $e->getMessage();
            return $error;
        }
        $sessionId = LoginFacade::formalLoginAction($uid,$insertData['utype'],$insertData['username'],$insertData['agentid'],$insertData['mobile']);
        return [
            'session_id' => $sessionId,
        ];
    }

    /**
     * 插入用户信息
     */
    private static function insertTradeMember($agentid,$mobile,$password,$trackUid,$devPath)
    {
        $insertData = [
            'agentid'           => !empty($agentid) ? intval($agentid) : Config::get('definedconst.trade_member')['default_agentid'], //非经推荐，自然注册，则归属到总代理名下
            'mobile'            => $mobile,
            'userpwd'           => StringHelper::thinkUcenterMd5($password),
            'fundpwd'           => StringHelper::thinkUcenterMd5($password),
            'dev_uid'           => $trackUid,
            'utype'             => Config::get('definedconst.trade_member')['utype_member'],
            'username'          => $mobile,
            'dev_path'          => $devPath,
            'trade_fee_percent' => config::get('definedconst.trade_member')['goods_member_trade_fee_percentage'],
            'reg_ip'            => Request::instance()->ip(),
        ];
        $uid = TradeMemberCache::insertTradeMember($insertData);
        if(false === $uid){
            return LoginError::$LOGIN_TRADE_MEMBER_ADD_FAIL;
        }
        $insertData['uid'] = $uid;
        return $insertData;
    }

    /**
     * 插入用户资金信息
     */
    private static function insertTradeMemberFund($uid)
    {
        $insertFundData = [
            'uid'             => $uid,
            'money_begin'     => 0,
            'money_now'       => 0,
            'money_frozen'    => 0,
            'money_free'      => 0,
            'limit_out_money' => 0,
        ];
        $result = TradeMemberFundCache::insertTradeMemberFund($insertFundData);
        if(false === $result){
            return LoginError::$LOGIN_TRADE_MEMBER_FUND_ADD_FAIL;
        }
        return true;
    }

    /**
     * 插入用户批发零售通用券表
     */
    private static function insertTradeMemberInfo($uid,$trackUid)
    {
        $insertData = [
	        'uid' => $uid,
        ];
        if($trackUid > 0){  //被推广来注册，则给被推广的人发放商城消费券
            $insertData['consumer_coupon_amount'] = 300;
        }
        $result = TradeMemberInfoCache::addTradeMemberInfo($insertData);
        if(false === $result){
        	return LoginError::$LOGIN_TRADE_MEMBER_INFO_ADD_FAIL;
        }
        return true;
    }
}