<?php

/**
 * $Id: RequestHandleFacade.php 29 2016-11-26 05:53:55Z myc $
 */
namespace app\index\facade;
use app\core\Facade;
use think\Config;
use app\core\error\LoginError;
use app\data\dbcache\model\prime\TradeMemberCache;
use app\core\helper\StringHelper;
use app\core\helper\VcodeHelper;

class PasswordFacade extends Facade
{
    /**
     * 找回密码时检查商户信息
     */
    public static function checkUserInfo($mobile,$uid,$smsVcode)
    {
        $result = VcodeHelper::checkSmsVcode($mobile,$smsVcode);
        if(isset($result['code'])){
        	return $result;
        }
        $userInfo = TradeMemberCache::getTradeMemberInfoByUidAndMobile($uid,$mobile);
        if(empty($userInfo)){
            return LoginError::$LOGIN_WRONG_TRADE_MEMBER_INFO;
        }
        session(Config::get('GET_PWD_UID_SESSION'),$userInfo['uid']);

        return true;
    }

    /**
     * 检查用户名和密码
     */
    public static function getUserInfoByUserIdAndPassword($userId,$password)
    {
        $userInfo = TradeMemberCache::getTradeMemberInfoByUid($userId);
        if(empty($userInfo) || StringHelper::thinkUcenterMd5($password) != $userInfo['userpwd']){
            return LoginError::$LOGIN_WRONG_USERNAME_OR_PASSWORD;
        }

        return $userInfo;
    }

    /**
     * 商户信息检查通过后重置密码
     */
    public static function resetPwd($password,$cfmPassword)
    {
        if($password !== $cfmPassword){
            return LoginError::$LOGIN_WRONG_TWICE_PASSWORD;
        }
        $uid = session(Config::get('GET_PWD_UID_SESSION'));
        if(empty($uid)){
            return LoginError::$LOGIN_NO_GET_PWD_UID;
        }
        $newPassword = StringHelper::thinkUcenterMd5($password);
        $result = TradeMemberCache::setPwdByUid($newPassword,$uid);
        if(false === $result){
            return LoginError::$LOGIN_RESET_PWD_FAIL;
        }

        return true;
    }
}