<?php

/**
 * $Id: RequestHandleFacade.php 29 2016-11-26 05:53:55Z myc $
 */
namespace app\index\facade;
use app\core\Facade;
use think\Config;
use app\core\error\LoginError;
use app\data\dbcache\model\prime\SmsCheckHistoryCache;
use think\captcha\Captcha;
use app\core\helper\SmsHelper;
use app\data\dbcache\model\prime\SmsLogCache;

class VcodeFacade extends Facade
{
    /**
     * 发送短信验证码
     */
    public static function sendSmsVcode($vcode,$mobile,$skipvcode,$lineType)
    {
        if(!empty($lineType) && empty(Config::get('SMS_CONFIG_'.$lineType))){
            return LoginError::$LOGIN_WRONG_LINE_TYPE;
        }
        if(0 == $skipvcode){
            $captcha = new Captcha();
            $result = $captcha->check($vcode);
            if(false === $result){
                return LoginError::$LOGIN_WRONG_VERIFY_CODE;
            }
        }
        $todayTime = strtotime(date('Y-m-d',time()));  //今日开始的时间戳
        $count = SmsCheckHistoryCache::getSmsCheckHistoryCountByMobileAndAdddate($mobile,$todayTime);
        $smsConfigItem = ('' == $lineType) ? 'SMS_CONFIG' : 'SMS_CONFIG_'.$lineType;
        if($count > Config::get($smsConfigItem.'.MAX_DAY_APPLY_COUNTS')){
            return LoginError::$LOGIN_SMS_CODE_EXCEED_LIMIT;
        }
        //检查验证码
        $codeInfo = SmsCheckHistoryCache::getLatestSmsCheckHistoryByMobile($mobile);
        if(!empty($codeInfo)){
            if(time()-$codeInfo['adddate'] <= Config::get($smsConfigItem.'.MOB_REQUEST_INTERVAL')){
                $restTime = Config::get($smsConfigItem.'.MOB_REQUEST_INTERVAL') - (time()-$codeInfo['adddate']);
                $error = LoginError::$LOGIN_SMS_CODE_TOO_SHORT_INTERVAL;
                $error['info'] = vsprintf($error['info'],[$restTime]);
                return $error;
            }
        }
        $vcode = rand(200000,999999);
        $msg ="尊敬的用户，您的验证码是：$vcode";
        $returnInfo = SmsHelper::sendSms($mobile,$msg,$lineType);
        $lineTypeStr = Config::get('SMS_LINE_LIST.'.$lineType);
        $insertLogData = [
	        'mob'       => $mobile,
	        'msg'       => $msg,
	        'line_type' => $lineTypeStr,
	        'smslog'    => implode(' ',$returnInfo['info']),
        ];
        SmsLogCache::addSmsLog($insertLogData);
        if(0 == $returnInfo['status']){  //失败
            return LoginError::$LOGIN_SMS_VCODE_SEND_FAIL;
        }

        $insertHistoryData = [
            'mob'     => $mobile,
            'vcode'   => $vcode,
            'msg'     => $msg,
            'used'    => '0',
        ];
        SmsCheckHistoryCache::addSmsCheckHistory($insertHistoryData);

        return true;
    }
}