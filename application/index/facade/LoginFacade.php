<?php

/**
 * $Id: RequestHandleFacade.php 29 2016-11-26 05:53:55Z myc $
 */
namespace app\index\facade;
use app\core\Facade;
use think\Config;
use app\core\error\LoginError;
use app\data\dbcache\model\prime\TradeMemberCache;
use app\core\helper\StringHelper;
use app\core\helper\SignHelper;
use think\captcha\Captcha;
use think\Request;
use app\core\helper\HappyMaoRequestHelper;
use app\core\helper\RetailRequestHelper;

class LoginFacade extends Facade
{
    /**
     * 登录
     */
    public static function doLogin($username,$password,$vcode)
    {
        $captcha = new Captcha();
        $result = $captcha->check($vcode);
        if(false === $result){
        	return LoginError::$LOGIN_WRONG_VERIFY_CODE;
        }
        $userInfo = TradeMemberCache::getTradeMemberInfoByUsernameOrUidOrMobile($username);
        if(empty($userInfo) || StringHelper::thinkUcenterMd5($password) != $userInfo['userpwd']){
        	return LoginError::$LOGIN_WRONG_USERNAME_OR_PASSWORD;
        }
        if($userInfo['userstate']<=0){
            return LoginError::$LOGIN_WRONG_USER_STATE;
        }
        $result = self::formalLoginAction($userInfo['uid'],$userInfo['utype'],$userInfo['username'],$userInfo['agentid'],$userInfo['mobile'],$userInfo['realname']);

        return [
	        'session_id' => session_id(),
        ];
    }

    /**
     * 具体登录操作
     */
    public static function formalLoginAction($uid,$utype,$username,$agentid,$mobile,$realname='')
    {
        $sessionData = [
            'uid'     => $uid,
            'utype'   => $utype,
            'username'=> $username,
            'mobile'  => $mobile,
            'realname'=> $realname,
            'agentid' => $agentid,
            'sign'    => SignHelper::createUserInfoSign($uid,$username,$utype),
        ];
        session(Config::get('USER_SESSION'),$sessionData);  //存入session
        $ip = Request::instance()->ip();
        TradeMemberCache::setLoginCountsAndIpByUid($uid,$ip);
        return true;
    }
}