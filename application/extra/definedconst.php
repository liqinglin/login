<?php
/**
 * 常量定义类
 * $id
 */
return [
    'trade_member' => [
         /*** 默认的总代理*/
        'default_agentid' => 1000001,
        /*** 用户类型：普通用户*/
        'utype_member'    => 'member',
        /*** 手续费比例*/
        'goods_member_trade_fee_percentage' => 0.003,
    ],
];