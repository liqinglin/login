<?php
namespace app\data\model\semdb;
use app\core\ModelBase;
class TradeMemberFund extends ModelBase{
    public function insertTradeMemberFund($insertFundData)
    {
        $insertFundData['last_update'] = time();
        $insertFundData['adddate']     = time();
        return $this->insert($insertFundData);
    }
}