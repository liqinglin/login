<?php
namespace app\data\model\semdb;
use app\core\ModelBase;
class TradeMember extends ModelBase{

    public function getTradeMemberInfoByMobile($mobile)
    {
        $row = $this->where([
	        'mobile' => $mobile,
        ])->find();
        if(empty($row)){
        	return null;
        }

        return $row->toArray();
    }

    public function getTradeMemberInfoByUid($uid)
    {
        $row = $this->where([
                    'uid' => $uid,
                ])->find();
        if(empty($row)){
            return null;
        }

        return $row->toArray();
    }

    public function getTradeMemberInfoByUidAndMobile($uid,$mobile)
    {
        $row = $this->where([
	        'uid'    => $uid,
	        'mobile' => $mobile,
        ])->find();
        if(empty($row)){
        	return null;
        }

        return $row->toArray();
    }

    public function getTradeMemberInfoByUsernameOrUidOrMobile($username)
    {
        $row = $this->where([
	        'username' => $username,
        ])->whereOr([
	        'uid'      => intval($username),
        ])->whereOr([
	        'mobile'   => $username,
        ])->find();
        if(empty($row)){
        	return null;
        }

        return $row->toArray();
    }

    public function insertTradeMember($insertData)
    {
        $insertData['reg_date']  = time();
        $insertData['userstate'] = 1;
        $result = $this->insert($insertData);
        if(false === $result){
        	return $result;
        }

        return $this->getLastInsID();
    }

    public function setLoginCountsAndIpByUid($uid,$ip)
    {
        return $this->where([
	        'uid' => $uid,
        ])->update([
	        'login_counts'    => ['exp','login_counts+1'],
	        'last_login_date' => time(),
	        'last_login_ip'   => $ip,
        ]);
    }

    public function setPwdByUid($newPassword,$uid)
    {
        return $this->where([
            'uid' => $uid,
        ])->update([
            'userpwd' => $newPassword,
            'fundpwd' => $newPassword,
        ]);
    }
}