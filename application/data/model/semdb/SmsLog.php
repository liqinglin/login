<?php
namespace app\data\model\semdb;
use app\core\ModelBase;
class SmsLog extends ModelBase{

    public function addSmsLog($insertData)
    {
        $insertData['adddate'] = time();
        return $this->insert($insertData);
    }
}