<?php
namespace app\data\model\semdb;
use app\core\ModelBase;
class TradeAgent extends ModelBase{
    public function getTradeAgentInfoByAgentTradeUid($agentTradeUid)
    {
        $row = $this->where([
	        'agent_trade_uid' => $agentTradeUid,
        ])->find();
        if(empty($row)){
        	return null;
        }

        return $row->toArray();
    }

}