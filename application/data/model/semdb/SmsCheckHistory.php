<?php
namespace app\data\model\semdb;
use app\core\ModelBase;
class SmsCheckHistory extends ModelBase{
    public function addSmsCheckHistory($insertData)
    {
        $insertData['adddate'] = time();
        return $this->insert($insertData);
    }

    public function getLatestSmsCheckHistoryByMobile($mobile)
    {
        $row = $this->where([
	        'mob' => $mobile,
        ])->order('adddate DESC')
          ->find();
        if(empty($row)){
        	return null;
        }

        return $row->toArray();
    }

    public function getSmsCheckHistoryCountByMobileAndAdddate($mobile,$adddate)
    {
        return $this->where([
	        'mob'     => $mobile,
	        'adddate' => ['egt',$adddate],
        ])->count();
    }
}