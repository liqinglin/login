<?php
namespace app\data\model\center;
use app\core\ModelBase;
class Oauth extends ModelBase{

    public function findServerByAppid($appid)
    {
        $row = $this->where([
            'oauth_appid' => $appid
        ])->find();
        if(empty($row)){
        	return null;
        }

        return $row->toArray();
    }
}