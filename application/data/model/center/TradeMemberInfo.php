<?php
namespace app\data\model\center;
use app\core\ModelBase;
class TradeMemberInfo extends ModelBase{
    public function addTradeMemberInfo($insertData)
    {
        $insertData['create_time'] = time();
        $insertData['update_time'] = time();
        return $this->insert($insertData);
    }

    public function incrConsumerCouponAmountByUid($consumerCouponAmount,$uid)
    {
        return $this->where([
            'uid' => $uid,
        ])->update([
            'consumer_coupon_amount' => ['exp','consumer_coupon_amount+'.$consumerCouponAmount],
            'update_time'            => time(),
        ]);
    }
}