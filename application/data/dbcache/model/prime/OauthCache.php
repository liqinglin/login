<?php
/**
 * 授权缓存
 * $Id$
 */
namespace app\data\dbcache\model\prime;
use app\core\CacheBase;
use app\data\model\center\Oauth;

class OauthCache extends CacheBase
{
    public static function findServerByAppid($appId)
    {
        $model = new Oauth();
        return $model->findServerByAppid($appId);
    }

}