<?php
/**
 * 用户批发零售通用券表缓存
 * $Id$
 */
namespace app\data\dbcache\model\prime;
use app\core\CacheBase;
use app\data\model\center\TradeMemberInfo;

class TradeMemberInfoCache extends CacheBase
{
    public static function addTradeMemberInfo($insertData)
    {
        return self::getTradeMemberInfoModel()->addTradeMemberInfo($insertData);
    }

    public static function incrConsumerCouponAmountByUid($consumerCouponAmount,$uid)
    {
        return self::getTradeMemberInfoModel()->incrConsumerCouponAmountByUid($consumerCouponAmount,$uid);
    }

    private static function getTradeMemberInfoModel()
    {
        return new TradeMemberInfo();
    }

}