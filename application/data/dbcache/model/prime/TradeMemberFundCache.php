<?php
/**
 * 用户表缓存
 * $Id$
 */
namespace app\data\dbcache\model\prime;
use app\core\CacheBase;
use app\data\model\semdb\TradeMemberFund;

class TradeMemberFundCache extends CacheBase
{
    public static function insertTradeMemberFund($insertFundData)
    {
        return self::getTradeMemberFundModel()->insertTradeMemberFund($insertFundData);
    }

    private static function getTradeMemberFundModel()
    {
        return new TradeMemberFund();
    }

}