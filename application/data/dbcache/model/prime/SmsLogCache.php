<?php
/**
 * 手机短信日志表缓存
 * $Id$
 */
namespace app\data\dbcache\model\prime;
use app\core\CacheBase;
use app\data\model\semdb\SmsLog;

class SmsLogCache extends CacheBase
{
    public static function addSmsLog($insertData)
    {
        return self::getSmsLogModel()->addSmsLog($insertData);
    }

    private static function getSmsLogModel()
    {
        return new SmsLog();
    }

}