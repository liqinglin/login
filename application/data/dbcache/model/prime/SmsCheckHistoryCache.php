<?php
/**
 * 用户表缓存
 * $Id$
 */
namespace app\data\dbcache\model\prime;
use app\core\CacheBase;
use app\data\model\semdb\SmsCheckHistory;

class SmsCheckHistoryCache extends CacheBase
{
    public static function addSmsCheckHistory($insertData)
    {
        return self::getSmsCheckHistoryModel()->addSmsCheckHistory($insertData);
    }

    public static function getLatestSmsCheckHistoryByMobile($mobile)
    {
        return self::getSmsCheckHistoryModel()->getLatestSmsCheckHistoryByMobile($mobile);
    }

    public static function getSmsCheckHistoryCountByMobileAndAdddate($mobile,$adddate)
    {
        return self::getSmsCheckHistoryModel()->getSmsCheckHistoryCountByMobileAndAdddate($mobile,$adddate);
    }

    private static function getSmsCheckHistoryModel()
    {
        return new SmsCheckHistory();
    }

}