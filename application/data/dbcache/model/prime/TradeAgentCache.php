<?php
/**
 * 用户表缓存
 * $Id$
 */
namespace app\data\dbcache\model\prime;
use app\core\CacheBase;
use app\data\model\semdb\TradeAgent;

class TradeAgentCache extends CacheBase
{
    public static function getTradeAgentInfoByAgentTradeUid($agentTradeUid)
    {
        return self::getTradeAgentModel()->getTradeAgentInfoByAgentTradeUid($agentTradeUid);
    }

    private static function getTradeAgentModel()
    {
        return new TradeAgent();
    }

}