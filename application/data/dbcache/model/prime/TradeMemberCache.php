<?php
/**
 * 用户表缓存
 * $Id$
 */
namespace app\data\dbcache\model\prime;
use app\core\CacheBase;
use app\data\model\semdb\TradeMember;

class TradeMemberCache extends CacheBase
{
    public static function getTradeMemberInfoByMobile($mobile)
    {
        return self::getTradeMemberModel()->getTradeMemberInfoByMobile($mobile);
    }

    public static function getTradeMemberInfoByUid($uid)
    {
        return self::getTradeMemberModel()->getTradeMemberInfoByUid($uid);
    }

    public static function getTradeMemberInfoByUidAndMobile($uid,$mobile)
    {
        return self::getTradeMemberModel()->getTradeMemberInfoByUidAndMobile($uid,$mobile);
    }

    public static function getTradeMemberInfoByUsernameOrUidOrMobile($username)
    {
        return self::getTradeMemberModel()->getTradeMemberInfoByUsernameOrUidOrMobile($username);
    }

    public static function insertTradeMember($insertData)
    {
        return self::getTradeMemberModel()->insertTradeMember($insertData);
    }

    public static function setLoginCountsAndIpByUid($uid,$ip)
    {
        return self::getTradeMemberModel()->setLoginCountsAndIpByUid($uid,$ip);
    }

    public static function setPwdByUid($newPassword,$uid)
    {
        return self::getTradeMemberModel()->setPwdByUid($newPassword,$uid);
    }

    public static function getTradeMemberModel()
    {
        return new TradeMember();
    }

}