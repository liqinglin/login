<?php
/**
 * 授权缓存Key 管理
 * User: 洪吕石
 * Date: 2016/11/1
 * Time: 20:26
 */
namespace app\data\dbcache\key;
class OauthKeys extends \app\core\CacheKeyBase
{
    /**
     * appid缓存
     * @var array
     */
    public static $OAUTH_SERVER_INFO = [
        'key' => 'server_%s',
        'ttl' => 10,
    ];
}