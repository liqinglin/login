<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

/**
thinkphp->library->think->db->Connection.php
vendor->topthink->think-mongo->src->Connection.php

'oauth' => [
    'type'           => 'mysql',      // 数据库类型
    'hostname'       => '127.0.0.1',  // 服务器地址
    'database'       => 'coobar_sso', // 数据库名
    'username'       => 'cbsso',      // 用户名
    'password'       => '123456',     // 密码
    'hostport'       => '',           // 端口
    'params'         => [],           // 数据库连接参数
    'charset'        => 'utf8',       // 数据库编码默认采用utf8
    'prefix'         => 'cb_',        // 数据库表前缀
    'debug'          => true,         // 数据库调试模式
    'deploy'         => 0,            // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
    'rw_separate'    => false,        // 数据库读写是否分离 主从式有效
    'master_num'     => 1,            // 读写分离后 主服务器数量
    'slave_no'       => '',           // 指定从服务器序号
    'fields_strict'  => true,         // 是否严格检查字段是否存在
    'resultset_type' => 'array',      // 数据集返回类型 array 数组 collection Collection对象
    'auto_timestamp' => false,        // 是否自动写入时间戳字段
    'sql_explain'    => false,        // 是否需要进行SQL性能分析
],
 */
return [
    'center' => [
        'type'           => 'mysql',
        'hostname'       => '127.0.0.1',
        'database'       => 'sem_center',
        'username'       => 'root',
        'password'       => '123456',
        'hostport'       => 3306,
        'charset'        => 'utf8',
        'prefix'         => 'o_',
        'debug'          => true,
        'sql_explain'    => true,
    ],
    'semdb' => [
        'type'           => 'mysql',
        'hostname'       => '127.0.0.1',
        'database'       => 'semdb',
        'username'       => 'root',
        'password'       => '123456',
        'hostport'       => 3306,
        'charset'        => 'utf8',
        'prefix'         => 'o_',
        'debug'          => true,
        'sql_explain'    => true,
    ],
];
