/* FF下实现的currentStyle */
(function (b) {
    if (b) {
        HTMLElement.prototype.__defineGetter__("currentStyle", function () {
            return this.ownerDocument.defaultView.getComputedStyle(this, null);
        });
    }
})(/Firefox/.test(window.navigator.userAgent));
/*
 @ code by littlehand
 @ qq 29290052
 */
var __agent = window.navigator.userAgent.toLowerCase().replace(/\s/g, "");
var __agent_key = ["ie", "msie6", "msie7", "firefox", "opera", "safari", "chrome"];
var browser = new Object();
for (i = 0; i < __agent_key.length; i++) browser["is" + __agent_key[i].toString()] = __agent.indexOf(__agent_key[i]) != -1 ? true : false;
if (browser.issafari && browser.ischrome) browser.issafari = false;

var StringEx = {
    ToUnicode: function (str) {
        return escape(str).toLocaleLowerCase().replace(/%u/gi, '\\u');
    }
    ,
    ToGB2312: function (str) {
        return unescape(str.replace(/\\u/gi, '%u'));
    }
};

function getjsonmsg(data) {
    try {
        eval("var data=" + data);
        return data;
    }
    catch (ee) {
        return {};
    }
}

function mkparas(paras) {
    var arr = [];
    for (var p in paras) {
        arr.push(p + "=" + paras[p]);
    }
    return arr.join("&");
}

function addEvent(o, t, f) {
    if (window.addEventListener) {
        o.addEventListener(t, f, false);
    } else {
        o.attachEvent("on" + t, f);
    }
}

function bindEvent(o, f, args) {
    f.call(o, args);
}

String.prototype.format = function () {
    var args = arguments;
    if (args.length == 0) {
        return this;
    }
    var reg = /{(\d+)?}/g;
    return this.replace(reg, function ($0, $1) {
        return args[parseInt($1)];
    })
}

/* 去除字符两边的空格 */
String.prototype.Trim = function () {
    if (typeof this != 'string') {
        return this;
    }
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
String.prototype.ZhLen = function () {
    var len = 0;
    if (this == null || this.length == 0)
        return 0;
    var str = this.replace(/(^\s*)|(\s*$)/g, "");
    for (i = 0; i < str.length; i++)
        if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128)
            len++;
        else
            len += 2;
    return len;
}

String.prototype.replaceAll = function (reallyDo, replaceWith, ignoreCase) {
    if (!RegExp.prototype.isPrototypeOf(reallyDo)) {
        return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi" : "g")), replaceWith);
    } else {
        return this.replace(reallyDo, replaceWith);
    }
}

function cutstr(str, len) {
    var str_length = 0;
    var str_len = 0;
    str_cut = new String();
    str_len = str.length;
    for (var i = 0; i < str_len; i++) {
        a = str.charAt(i);
        str_length++;
        if (escape(a).length > 4) {
            //中文字符的长度经编码之后大于4
            str_length++;
        }
        str_cut = str_cut.concat(a);
        if (str_length >= len) {
            str_cut = str_cut.concat("...");
            return str_cut;
        }
    }
    //如果给定字符串小于指定长度，则返回源字符串；
    if (str_length < len) {
        return str;
    }
}

/* 对时间对象的扩展 */
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1,   //month
        "d+": this.getDate(),         //day
        "h+": this.getHours(),       //hour
        "m+": this.getMinutes(),   //minute
        "s+": this.getSeconds(),   //second
        "q+": Math.floor((this.getMonth() + 3) / 3),     //quarter
        "S": this.getMilliseconds()   //millisecond
    }
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] :
                ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};

Date.Now = {
    toString: function (f) {
        return new Date().format(f);
    }
}
/* 缩略图 */
function thumbImg(ImgD, width_s, height_s) {
    var image = new Image();
    image.src = ImgD.src;
    if (image.width > 0 && image.height > 0) {
        flag = true;
        if (image.width / image.height >= width_s / height_s) {
            if (image.width > width_s) {
                ImgD.width = width_s;
                ImgD.height = (image.height * width_s) / image.width;
            } else {
                ImgD.width = image.width;
                ImgD.height = image.height;
            }
        }
        else {
            if (image.height > height_s) {
                ImgD.height = height_s;
                ImgD.width = (image.width * height_s) / image.height;
            } else {
                ImgD.width = image.width;
                ImgD.height = image.height;
            }
        }
    }
}

/* 获取URL参数 request('参数名称') */
function request(p, inurl) {
    var ptn, url, paras;
    url = location.href;
    if (inurl) {
        url = inurl;
    }
    if (url.indexOf("?") == -1) return "";
    paras = url.split('?')[1];
    if (paras.indexOf(p + "=") == -1) return "";
    ptn = new RegExp(p + "=([^&]*)", "g");
    ptn.exec(paras);
    return RegExp.$1.replace("#", "");
}

function enterPostForm(form, input) {
    $(input).keydown(function (e) {
        var curKey = e.which;
        if (curKey == 13) {
            form.submit();
        }
    });
}

function verflash(src) {
    var ver = "";
    if (src.indexOf(".youku.com") != -1) {
        ver = "youku";
    }
    if (src.indexOf(".yy.com") != -1) {
        ver = "yy";
    }
    return ver;
}

function mkparas(paras) {
    var arr = [];
    for (var p in paras) {
        arr.push(p + "=" + paras[p]);
    }
    return arr.join("&");
}

function playYYVideo(id, options) {
    var tpl = '<embed id="{0}" {1} quality="high" align="middle" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" type="application/x-shockwave-flash" autostart="true">';
    var attrs = [];
    for (var p in options) {
        attrs.push('{0}="{1}"'.format(p, options[p]));
    }

    var html = tpl.format(id, attrs.join(" "));
    if (options.returnHTML) {
        return html;
    }
    else {
        document.write(html);
    }
}

function playflash(id, src, w, h, paras, isreturn) {
    if (!paras) paras = "";
    function splace(str, a, b) {
        return str.split(a).join(b);
    }

    var s = [];
    s.push("<object id='{0}' name='{0}' width='{1}' height='{2}' align='middle'  codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7' classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'>");
    s.push("<param value='always' name='allowScriptAccess' />");
    s.push("<param value='high' name='quality' />");
    s.push("<param value='transparent' name='wmode' />");
    s.push("<param value='{3}' name='movie' />");
    s.push("<param value='{4}' name='flashvars' />");
    s.push("<param value='true' name='allowFullScreen' />");

    s.push("<embed id='{0}' name='{0}' ");
    s.push("	width='{1}' ");
    s.push("	height='{2}' ");
    s.push("	align='middle' ");
    s.push("	pluginspage='http://www.macromedia.com/go/getflashplayer'");
    s.push("	type='application/x-shockwave-flash' ");
    s.push("	allowscriptaccess='always' ");
    s.push("	name='banner'");
    s.push("	wmode='transparent' ");
    s.push("	quality='high' ");
    s.push("	allowFullScreen='true' ");
    s.push("	flashvars='{4}'");
    s.push("	src='{3}' />");
    s.push("</object>");

    var html = s.join("");
    html = splace(html, "{0}", id);
    html = splace(html, "{1}", w);
    html = splace(html, "{2}", h);
    html = splace(html, "{3}", src);
    html = splace(html, "{4}", paras);

    if (isreturn) {
        return html;
    }
    document.write(html);
}
function playYouku(src, opts) {
    var defaults = $.extend({
        width: 400,
        height: 300,
        autoplay: false,
        tpl: "{0}",
        write: true
    }, opts);

    var str = "";
    str += '<embed ';
    str += 'src="{0}"'
        .format(src);
    str += 'quality="high" ';
    str += 'wmode="transparent" ';
    str += 'width="{0}" '
        .format(defaults.width);
    str += 'height="{0}" '
        .format(defaults.height);
    str += 'align="middle" ';
    str += 'allowscriptaccess="sameDomain"';
    str += 'type="application/x-shockwave-flash" ';
    str += 'flashvars="isShowRelatedVideo=false&isAutoPlay={0}&isDebug=false&UserID=0&RecordCode=1001,1002,1003,1004,1005,1006,2001,3001,3002,3003,3004,3005,3007,3008,9999&RecordResource=index&isLoop=false&winType=index&playMovie=true&MMControl=true&MMout=true&show_type=3" '
        .format(defaults.autoplay.toString());
    str += '>';
    str += '</embed>';

    var html = defaults.tpl.format(str);
    if (defaults.write) {
        document.write(html);
    }
    return html;
}
function playMovie(url, autoplay, opts) {

    if (!autoplay) {
        autoplay = false;
    }

    var defaults = $.extend({
        width: 243,
        height: 155,
        returnHtml: false,
        scaleMode: "exactFil",
        isLoadBegin: "false",
        controlPanelBgColor: "0x986f39",
        controlPanelBtnColor: "0xffffff",
        contralPanelBtnGlowColro: "0xffff00"
    }, opts);
    if (/\.swf$/.test(url)) {
        var html = playflash("site_home_video", url, defaults.width, defaults.height, "", true);
        if (defaults.returnHtml) {
            return html;
        }
        else {
            document.getElementById("video_content").innerHTML = html;
        }
    }

    var flashvars = "xml=" +
        "<vcastr>" +
        "<channel>" +
        "<item>" +
        "<source>{url}</source>" +
        "<duration></duration>" +
        "<title></title>" +
        "</item>" +
        "</channel>" +
        "<config>" +
        (autoplay == true ? "<isAutoPlay>true</isAutoPlay>" : "<isAutoPlay>false</isAutoPlay>")
        +
        "<bufferTime>2</bufferTime>" +
        "<contralPanelAlpha>0.75</contralPanelAlpha>" +
        "<controlPanelBgColor>" + defaults.controlPanelBgColor + "</controlPanelBgColor>" +
        "<controlPanelBtnColor>" + defaults.controlPanelBtnColor + "</controlPanelBtnColor>" +
        "<contralPanelBtnGlowColro>" + defaults.contralPanelBtnGlowColro + "</contralPanelBtnGlowColro>" +
        "<controlPanelMode>bottom</controlPanelMode>" +
        "<defautVolume>0.7</defautVolume>" +
        "<isLoadBegin>" + defaults.isLoadBegin + "</isLoadBegin>" +
        "<isShowAbout>false</isShowAbout>" +
        "<scaleMode>" + defaults.scaleMode + "</scaleMode>" +
        "<isRepeat>false</isRepeat>" +
        "</config>" +
        "</vcastr>";

    //alert(flashvars);
    flashvars = flashvars.replace("{url}", url);
    var html = playflash("site_home_video", "/images/vcastr3.swf", defaults.width, defaults.height, flashvars, true);
    if (defaults.returnHtml) {
        return html;
    }
    else {
        document.getElementById("video_content").innerHTML = html;
    }
    return false;
}

function SwitchExecute(v, cs) {
    var defaultF, targetF;
    $.each(cs, function (i, c) {
        if (v.toString() == c[0].toString()) {
            targetF = c[1];
            return false;
        }
        if (c[0] == "default") {
            defaultF = c[1];
        }
    })

    if (typeof (targetF) == 'function') {
        targetF();
        return;
    }
    else {
        defaultF();
    }
}

function GetCookie(name) {
    var bikky = document.cookie;
    name += "=";
    var i = 0;
    while (i < bikky.length) {
        var offset = i + name.length;
        if (bikky.substring(i, offset) == name) {
            var endstr = bikky.indexOf(";", offset);
            if (endstr == -1) endstr = bikky.length;
            return unescape(bikky.substring(offset, endstr));
        }
        i = bikky.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

function SetCookie(name, value, days) {
    var Days = 30; //此 cookie 将被保存 30 天
    if (days) {
        Days = days;
    }
    var exp = new Date(); //new Date("December 31, 9998");
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

//设置世界时间,整站通用
function __setTime() {

    function _s(i) {
        return document.getElementById(i);
    }

    var myDate = new Date();
    try {
        //瑞士
        myDate = new Date();
        myDate.setHours(myDate.getHours() - 7);
        _s("spanRS").innerHTML = myDate.format("hh:mm:ss");

        //伦敦
        myDate = new Date();
        myDate.setHours(myDate.getHours() - 8);
        _s("spanLD").innerHTML = myDate.format("hh:mm:ss");

        //纽约
        myDate = new Date();
        myDate.setHours(myDate.getHours() - 12);
        _s("spanNY").innerHTML = myDate.format("hh:mm:ss");

        //东京
        myDate = new Date();
        myDate.setHours(myDate.getHours() + 1);
        _s("spanDJ").innerHTML = myDate.format("hh:mm:ss");

        //香港
        myDate = new Date();
        myDate.setHours(myDate.getHours());
        _s("spanXN").innerHTML = myDate.format("hh:mm:ss");
    }
    catch (ee) {
        window.clearTimeout(window.mmglobaltimes);
    }
    window.mmglobaltimes = window.setTimeout(__setTime, 1000);
}

$(__setTime);

function loaduserstate() {
    var box = $("#bj_mains");
    if (box.size() == 0) return;

    $.get("/ajax.ashx", {t: "get_user_state", r: Math.random()}, function (r) {
        box.find("li:first").before("<li>" + r + "</li>");
    })
}

function getJqueryOuterHtml($dom) {
    return $($('<div></div>').html($dom.clone())).html();
}

function extend(des, src) {
    if (!des) {
        des = {};
    }
    if (src) {
        for (var i in src) {
            des[i] = src[i];
        }
    }
    return des;
}
function MagicColor(elem, colors, interTime) {
    this.elements = elem || [];
    this.colors = colors || ["#CC0000", "#CC6D00", "#CCCC00", "#00CC00", "#0000CC", "#00CCCC", "#CC00CC"];
    this.indexColors = 0;
    this.interTime = interTime;
}
extend(MagicColor.prototype, {
    timerCall: function () {
        var elem = this.elements;
        var color = this.colors[(this.indexColors++) % this.colors.length];
        for (var i = 0; i < elem.length; i++) {
            elem[i].style.color = color;
        }
    },
    reset: function () {
        if (this.timer) {
            clearInterval(this.timer);
            delete this.timer;
        }
    },
    start: function () {

        this.reset();
        var self = this;
        this.timer = setInterval(function () {
            self.timerCall();
        }, this.interTime || 100);
    }
});

(function ($) {
    $.fn.center = function (settings) {
        var style = $.extend({
            position: 'absolute', //absolute or fixed
            top: '50%',            //50%即居中，将应用负边距计算，溢出不予考虑了。
            left: '50%',
            zIndex: 2009,
            relative: true        //相对于包含它的容器居中还是整个页面
        }, settings || {});

        return this.each(function () {
            var $this = $(this);

            if (style.top == '50%') style.marginTop = -$this.outerHeight() / 2;
            if (style.left == '50%') style.marginLeft = -$this.outerWidth() / 2;
            if (style.relative && !$this.parent().is('body') && $this.parent().css('position') == 'static')
                $this.parent().css('position', 'relative');
            delete style.relative;
            //ie6
            if (style.position == 'fixed' && $.browser.version == '6.0') {
                style.marginTop += $(window).scrollTop();
                style.position = 'absolute';
                $(window).scroll(function () {
                    $this.stop().animate({
                        marginTop: $(window).scrollTop() - $this.outerHeight() / 2
                    });
                });
            }

            $this.css(style);
        });
    };
})(jQuery);

/* 向 textarea插入字符 */
function insertToTextArea(elementId, tag) {
    var myField;
    myField = document.getElementById(elementId);
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = tag;
        myField.focus();
    }
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        var cursorPos = endPos;
        myField.value = myField.value.substring(0, startPos)
            + tag
            + myField.value.substring(endPos, myField.value.length);
        cursorPos += tag.length;
        myField.focus();
        myField.selectionStart = cursorPos;
        myField.selectionEnd = cursorPos;
    }
    else {
        myField.value += tag;
        myField.focus();
    }
}

function setCaretPosition(tObj, sPos) {
    if (tObj.setSelectionRange) {
        setTimeout(function () {
            tObj.setSelectionRange(sPos, sPos);
            tObj.focus();
        }, 0);
    } else if (tObj.createTextRange) {
        var rng = tObj.createTextRange();
        rng.move('character', sPos);
        rng.select();
    }
}

if (!window.console) {
    window.console = {
        log: function () {

        }
    }
}

function requireElements(_selector, callback) {
    var elements = $(_selector);
    if (elements.size() > 0) {
        callback.call(null, elements);
    }
}
jQuery.fn.outerHTML = function (s) {
    return $("<div></div>").append($(this).clone()).html();
}

$.fn.ctrlEnter = function (btns, fn) {
    var thiz = $(this);
    btns = $(btns);

    function performAction(e) {
        fn.call(thiz, e);
    };
    thiz.bind("keydown", function (e) {
        if (e.keyCode === 13 && e.ctrlKey) {
            performAction(e);
            e.preventDefault(); //阻止默认回车换行
        }
    });
    btns.bind("click", performAction);
}

$.fn.inputTip = function () {

    var inputBlur = function () {
        $(this).addClass("onblur");
        var defaultValue = $(this).attr("defaultVal");
        if (defaultValue == null || defaultValue == "") {
            return;
        }

        if (defaultValue != "" && this.value == "") {
            if ($(this).attr("type").toString().toLowerCase() != "password") {
                this.value = defaultValue;
            }
        }
        else {
            $(this).removeClass("onblur");
        }
    }

    var inputFocus = function () {
        var defaultValue = $(this).attr("defaultVal");
        if (defaultValue != "" && this.value == defaultValue) {
            this.value = "";
        }
        $(this).removeClass("onblur");
    }

    var defaultValueFunction = function () {
        var defaultValue = $(this).attr("defaultVal");
        if (defaultValue == null || defaultValue == "") {
            return;
        }
        if (!!defaultValue && defaultValue.toString().length > 0) {
            return defaultValue;
        }
        return this.value;
    }

    return this.each(function () {
        $(this).blur(inputBlur).focus(inputFocus).val(defaultValueFunction.call(this));
    });
}

function img_async_load(url, callback) {
    var img = new Image();
    img.src = url;
    if (img.complete) {
        callback(img.width, img.height);
    } else {
        img.onload = function () {
            callback(img.width, img.height);
            img.onload = null;
        };
    }
    ;

};

function thumb_image_size(ImgD, FitWidth, FitHeight) {
    var image = new Image();
    image.src = ImgD.src;

    if (image.width > 0 && image.height > 0) {
        if (image.width / image.height >= FitWidth / FitHeight) {
            //alert(image.height * FitWidth);
            //alert(image.width+'/'+image.height);

            if (image.width > FitWidth) {
                $(ImgD).css({
                    width: FitWidth,
                    height: (image.height * FitWidth) / image.width
                });
            }
            else {

                $(ImgD).css({
                    width: image.width,
                    height: image.height
                });
            }
        }
        else {

            if (image.height > FitHeight) {

                $(ImgD).css({
                    width: FitHeight,
                    height: (image.width * FitHeight) / image.height
                });

            }
            else {
                $(ImgD).css({
                    width: image.width,
                    height: image.height
                });
            }
        }
    }
}

/**
 * 加法运算，避免数据相加小数点后产生多位数和计算精度损失。
 *
 * @param num1加数1 | num2加数2
 */
function numAdd(num1, num2) {
    var baseNum, baseNum1, baseNum2;
    try {
        baseNum1 = num1.toString().split(".")[1].length;
    } catch (e) {
        baseNum1 = 0;
    }
    try {
        baseNum2 = num2.toString().split(".")[1].length;
    } catch (e) {
        baseNum2 = 0;
    }
    baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
    return (num1 * baseNum + num2 * baseNum) / baseNum;
};
/**
 * 加法运算，避免数据相减小数点后产生多位数和计算精度损失。
 *
 * @param num1被减数  |  num2减数
 */
function numSub(num1, num2) {
    var baseNum, baseNum1, baseNum2;
    var precision;// 精度
    try {
        baseNum1 = num1.toString().split(".")[1].length;
    } catch (e) {
        baseNum1 = 0;
    }
    try {
        baseNum2 = num2.toString().split(".")[1].length;
    } catch (e) {
        baseNum2 = 0;
    }
    baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
    precision = (baseNum1 >= baseNum2) ? baseNum1 : baseNum2;
    return ((num1 * baseNum - num2 * baseNum) / baseNum).toFixed(precision);
};
/**
 * 乘法运算，避免数据相乘小数点后产生多位数和计算精度损失。
 *
 * @param num1被乘数 | num2乘数
 */
function numMulti(num1, num2) {
    var baseNum = 0;
    try {
        baseNum += num1.toString().split(".")[1].length;
    } catch (e) {
    }
    try {
        baseNum += num2.toString().split(".")[1].length;
    } catch (e) {
    }
    return Number(num1.toString().replace(".", "")) * Number(num2.toString().replace(".", "")) / Math.pow(10, baseNum);
};
/**
 * 除法运算，避免数据相除小数点后产生多位数和计算精度损失。
 *
 * @param num1被除数 | num2除数
 */
function numDiv(num1, num2) {
    var baseNum1 = 0, baseNum2 = 0;
    var baseNum3, baseNum4;
    try {
        baseNum1 = num1.toString().split(".")[1].length;
    } catch (e) {
        baseNum1 = 0;
    }
    try {
        baseNum2 = num2.toString().split(".")[1].length;
    } catch (e) {
        baseNum2 = 0;
    }
    with (Math) {
        baseNum3 = Number(num1.toString().replace(".", ""));
        baseNum4 = Number(num2.toString().replace(".", ""));
        return (baseNum3 / baseNum4) * pow(10, baseNum2 - baseNum1);
    }
};


// 一些工具类 依赖jquery
(function (w, $) {
    w.common = {
        // ajax基于deferred对象的封装 可根据实际情况决定是否接管fail事件
        ajax: function (url, param, type) {
            return $.ajax({
                url: url,
                data: param || {},
                type: type || 'GET',
            }).then(function (resp) {
                if (typeof resp == 'string')
                    resp = JSON.parse(resp);
                if (resp.status == "1")
                    return resp;
                else {
                    $('.verifyimg').trigger('click');
                    alert(resp.info);
                    return $.Deferred().reject(resp.code);
                }
            });
        },

        // 事件绑定
        bind: function (events) {
            $.each(events, function (i, n) {
                var arr = i.split(' ');
                $('body').on(arr[1], arr[0], n);
            });
        },

        // 解析query字符串为对象
        queryResolve: function () {
            var queryObj = {}; // query对象
            var arr = [];
            var queryStr = decodeURIComponent(window.location.search).substring(1);
            if (queryStr) {
                if (queryStr.indexOf('&') > -1)
                    arr = queryStr.split('&');
                else
                    arr.push(queryStr);
                $.each(arr, function (i, n) {
                    var kv = n.split('=');
                    queryObj[kv[0]] = isNaN(kv[1] * 1) ? kv[1] : kv[1] * 1;
                });
            }
            return queryObj;
        },

        //获取url传参
        GetQueryString: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null)return unescape(r[2]);
            return null;
        },

        // 拼接query对象为字符串
        queryParse: function (obj) {
            var arr = [];
            $.each(obj, function (i, n) {
                arr.push(i + '=' + n);
            });
            return '?' + arr.join('&');
        },

        // 获取session
        get: function (name) {
            return sessionStorage.getItem(name);
        },

        // 设置session
        set: function (key, value) {
            return sessionStorage.setItem(key, value);
        }
    };
})(window, $);