/**
 * Created by Administrator on 2017/5/24.
 */
$(function () {
    var track_uid = common.GetQueryString('track_uid');

    layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form()
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;

        form.on('submit(mform)', function (data) {
            //layer.alert(JSON.stringify(data.field), {title: '最终的提交信息'});
            if (!data.field.mobile) {
                layer.msg('请填写您的手机号码');
            } else if (!data.field.userpwd) {
                layer.msg('请填写密码');
            } else if (!data.field.repwd) {
                layer.msg('请填写确认密码');
            } else if (!data.field.mobcode) {
                layer.msg('请填写手机验证码');
            } else if (data.field.isagree == 0) {
                layer.msg('您必须同意《商城用户注册协议》');
            } else {
                window.defaultModal.getRegister({
                    mobile: data.field.mobile,
                    oauth_appid: 'happymaoappid',
                    password: data.field.userpwd,
                    cfm_password: data.field.repwd,
                    sms_vcode: data.field.mobcode,
                    is_agreed: data.field.isagree,
                    track_uid: track_uid
                }).done(function (resp) {
                    layer.msg('恭喜您注册成功');
                    setTimeout(function () {
                        var pifaUrl = defaultModal.dev_happymao_com + '/trader.php?s=/web/client.html';
                        var loginHref = url ? url : pifaUrl;
                        if (loginHref.indexOf("?") > -1) {
                            location.href = loginHref + '&session_id=' + session_id;
                        } else {
                            location.href = loginHref + '?session_id=' + session_id;
                        }
                    }, 1800);
                });
            }
        });

        form.on('checkbox(agree)', function (data) {
            if (!data.elem.checked) {
                $('#isagree').val(0);
            }
            else {
                $('#isagree').val(1);
            }
        });

    });

    function send_code_callback(button) {
        var $ = jQuery;
        var disableSecond = parseInt("60");
        $(button).blur().addClass('disable').attr('disablesecond', disableSecond).html('{0} 秒后获取'.format(disableSecond));
        var disableTimer = setInterval(function () {
            var second = parseInt(button.attr('disablesecond'));
            if (second - 1 <= 0) {
                button.html('获取验证码').removeClass('disable');
                clearInterval(disableTimer);
                return;
            }
            button.attr('disableSecond', second - 1).html('{0} 秒后获取'.format(second - 1));
        }, 1000);
    }

    requireElements('.register-form', function (ele) {
        ele.find('a.get_mob_code').click(function () {
            var button = $(this);
            var mobile = $("#mobile").val();
            var vcode = $("#vcode").val();
            var skip_vcode = '';
            if (!mobile) {
                alert('请输入您的手机号码')
            } else if (!vcode) {
                alert('请输入验证码')
            } else {
                window.defaultModal.getVcode(mobile, skip_vcode, vcode)
                    .done(function (resp) {
                        alert('验证码已经发到您的手机,请查收');
                        send_code_callback.call(button, button);
                    });
            }
        });

        ele.find('a.show-register-rule').click(function () {

            var content = $('#register-rule-text').html();
            content = content.split("\n");
            var text = [];
            $.each(content, function (i, item) {
                text.push("<p>" + item + "</p>");
            })
            var layer = layui.layer;
            layer.open({
                type: 1
                , title: false
                , closeBtn: false
                , area: ['80%', '60%']
                , shade: 0.8
                , skin: 'register-rule-dialog'
                , btn: ['关闭']
                , moveType: 1
                , content: text.join('')
                , success: function (layero) {
                    var buttonGroup = layero.find('.layui-layer-btn');
                    buttonGroup.attr({href: 'javascript:;'}).css('text-align', 'center');
                    buttonGroup.find('.layui-layer-btn0').click(function () {
                        $('#isagree').attr('checked', true).trigger('click');
                    });
                }
            });

        })
    });

    var rand = Date.parse(new Date());
    $('#captcha').attr('src', defaultModal.dev_login_com + '/pub/captcha?oauth_appid=happymaoappid&rand=' + rand)
        .on("click", function () {
            rand = Date.parse(new Date());
            $(this).attr('src', defaultModal.dev_login_com + '/pub/captcha?oauth_appid=happymaoappid&rand=' + rand);
        });
});