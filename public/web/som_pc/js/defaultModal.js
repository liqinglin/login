
(function (win, $, c) {
    var domain = 'http://login.semstq.com';
    // var domain = 'http://sw.semstq.com:2680';
    // var domain = 'http://192.168.1.229:8002';
    win.defaultModal = {

        dev_happymao_com: 'http://happymao.semstq.com',
        dev_login_com: 'http://login.semstq.com',
        // dev_happymao_com: 'http://sw.semstq.com:2681',
        // dev_login_com: 'http://sw.semstq.com:2680',
        // dev_happymao_com: 'http://192.168.1.229',
        // dev_login_com: 'http://192.168.1.229:8002',
        // dev_retail_com:'http://shop.semstq.com',

        /**
         *
         * @param password
         * @param username
         * @param vcode
         * @returns {*}
         */
        login: function (username, password, vcode,channel) { //登录的接口
            return c.ajax(domain + '/login/login', {
                oauth_appid: 'happymaoappid',
                password: password,
                username: username,
                vcode: vcode,
                channel:channel
            }, 'post');
        },

        /**
         *
         * @param rand
         * @returns {*}
         */
        getCaptcha: function (rand) { //获取图形验证码的接口
            return c.ajax(domain + '/pub/captcha', {
                oauth_appid: 'happymaoappid',
                rand: rand
            });
        },

        /**
         *
         * @param mobile
         * @param skip_vcode
         * @param vcode
         * @returns {*}
         */
        getVcode: function (mobile, skip_vcode, vcode) { //发送手机短信验证码的接口
            return c.ajax(domain + '/pub/smsvcode', {
                line_type: 'SEMB',
                mobile: mobile,
                oauth_appid: 'happymaoappid',
                skip_vcode: skip_vcode,
                vcode: vcode
            }, 'post');
        },

        /**
         *
         * @param mobile
         * @param sms_vcode
         * @param uid
         * @returns {*}
         */
        checkuserinfo: function (mobile, sms_vcode, uid) { // 找回密码时检查商户信息
            return c.ajax(domain + '/password/checkuserinfo', {
                mobile: mobile,
                oauth_appid: 'happymaoappid',
                sms_vcode: sms_vcode,
                uid: uid
            });
        },


        /**
         *
         * @param cfm_password
         * @param password
         * @returns {*}
         */
        getPassword: function (cfm_password, password) { //重置密码
            return c.ajax(domain + '/password/password', {
                cfm_password: cfm_password,
                oauth_appid: 'happymaoappid',
                password: password
            }, 'put');
        },

        /**
         *
         * @param param
         */
        getRegister: function (param) { //注册用户的接口
            return c.ajax(domain + '/register/register', param, 'post');
        }
    }
}(window, $, common));