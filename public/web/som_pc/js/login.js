/**
 * Created by Administrator on 2017/5/24.
 */

$(function () {

    var channel = common.GetQueryString('channel');
    var url = common.GetQueryString('url');
    layui.use(['form', 'layedit', 'laydate'], function () {
        function showLoginRule(session_id) {
            var layer = layui.layer;
            layer.open({
                type: 1
                , title: false
                , closeBtn: false
                , area: '600px'
                , shade: 0.8
                , skin: 'login-rule-dialog'
                , btn: ['拒绝接受', '我已阅读以上内容并自愿承担相关风险']
                , moveType: 1
                , content: $('#rule-dialog-content').html()
                , success: function (layero) {
                    var buttonGroup = layero.find('.layui-layer-btn');
                    buttonGroup.attr({href: 'javascript:;'}).css('text-align', 'center');
                    buttonGroup.find('.layui-layer-btn0').click(function () {
                        window.location.reload();
                    });
                    buttonGroup.find('.layui-layer-btn1').click(function () {
                        var pifaUrl = defaultModal.dev_happymao_com + '/trader.php?s=/web/client.html';
                        var loginHref = url ? url : pifaUrl;
                        if (loginHref.indexOf("?") > -1) {
                            location.href = loginHref + '&session_id=' + session_id;
                        } else {
                            location.href = loginHref + '?session_id=' + session_id;
                        }
                    });
                }
            });
        }

        $('#login').click(function () {
            var form = $('.data-form');
            var username = $('#username').val();
            var password = $('#password').val();
            var vcode = $('#vcode').val();
            if (!username) {
                alert('请输入您的手机号码/商户账号');
            } else if (!password) {
                alert('请输入您的登录密码');
            } else if (!vcode) {
                alert('请输入验证码');
            } else {
                window.defaultModal.login(username, password, vcode)
                    .done(function (resp) {
                        showLoginRule(resp.data.session_id);
                    });
            }
        });
    });


    var rand = Date.parse(new Date());
    $('#captcha').attr('src', defaultModal.dev_login_com + '/pub/captcha?oauth_appid=happymaoappid&rand=' + rand)
        .on("click", function () {
            rand = Date.parse(new Date());
            $(this).attr('src', defaultModal.dev_login_com + '/pub/captcha?oauth_appid=happymaoappid&rand=' + rand);
        });

});

