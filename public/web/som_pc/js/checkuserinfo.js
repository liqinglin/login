/**
 * Created by Administrator on 2017/5/24.
 */
$(function () {
    layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form()
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;
        form.on('submit(mform)', function (data) {
            //layer.alert(JSON.stringify(data.field), {title: '最终的提交信息'});
            var mobile = data.field.mobile;
            var sms_vcode = data.field.mobcode;
            var uid = data.field.account;
            if (!mobile) {
                alert('请填写您的手机号码')
            } else if (!uid) {
                alert('请填写您的商户账号')
            } else if (!sms_vcode) {
                alert('请填写您的手机验证码')
            } else {
                window.defaultModal.checkuserinfo(mobile, sms_vcode, uid)
                    .done(function (resp) {
                        sessionStorage.setItem("uid", uid);
                        setTimeout(function () {
                            location.href = "resetpwd.html";
                        }, 2000);
                    });
            }
        });
    });
    function send_code_callback(button) {
        var $ = jQuery;
        var disableSecond = parseInt("60");
        $(button).blur().addClass('disable').attr('disablesecond', disableSecond).html('{0} 秒后获取'.format(disableSecond));
        var disableTimer = setInterval(function () {
            var second = parseInt(button.attr('disablesecond'));
            if (second - 1 <= 0) {
                button.html('获取验证码').removeClass('disable');
                clearInterval(disableTimer);
                return;
            }
            button.attr('disableSecond', second - 1).html('{0} 秒后获取'.format(second - 1));
        }, 1000);
    }

    requireElements('.register-form', function (ele) {
        ele.find('a.get_mob_code').click(function () {
            var button = $(this);
            var mobile = $("#mobile").val();
            var vcode = $("#vcode").val();
            var skip_vcode = '';
            if (!mobile) {
                alert('请输入您的手机号码')
            } else if (!vcode) {
                alert('请输入验证码')
            } else {
                window.defaultModal.getVcode(mobile, skip_vcode, vcode)
                    .done(function (resp) {
                        send_code_callback.call(button, button);
                    });
            }
        });

        ele.find('a.show-register-rule').click(function () {

            var content = $('#register-rule-text').html();
            content = content.split("\n");
            var text = [];
            $.each(content, function (i, item) {
                text.push("<p>" + item + "</p>");
            });
            var layer = layui.layer;
            layer.open({
                type: 1
                , title: false
                , closeBtn: false
                , area: ['80%', '60%']
                , shade: 0.8
                , skin: 'register-rule-dialog'
                , btn: ['关闭']
                , moveType: 1
                , content: text.join('')
                , success: function (layero) {
                    var buttonGroup = layero.find('.layui-layer-btn');
                    buttonGroup.attr({href: 'javascript:;'}).css('text-align', 'center');
                    buttonGroup.find('.layui-layer-btn0').click(function () {
                        $('#isagree').attr('checked', true).trigger('click');
                    });
                }
            });

        });
    });

    var rand = Date.parse(new Date());
    $('#captcha').attr('src', defaultModal.dev_login_com + '/pub/captcha?oauth_appid=happymaoappid&rand=' + rand)
        .on("click", function () {
            rand = Date.parse(new Date());
            $(this).attr('src', defaultModal.dev_login_com + '/pub/captcha?oauth_appid=happymaoappid&rand=' + rand);
        });
});
