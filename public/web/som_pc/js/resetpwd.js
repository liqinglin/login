/**
 * Created by Administrator on 2017/5/25.
 */
$(function () {
    var uid = sessionStorage.getItem("uid");
    $("#uid").html(uid);
    layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form()
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;
        form.on('submit(mform)', function (data) {
            //layer.alert(JSON.stringify(data.field), {title: '最终的提交信息'});
            var password = data.field.userpwd;
            var cfm_password = data.field.repwd;
            if (!password) {
                alert('请填写密码')
            } else if (!cfm_password) {
                alert('请填写确认密码')
            } else {
                window.defaultModal.getPassword(password, cfm_password)
                    .done(function (resp) {
                        sessionStorage.setItem("uid", uid);
                        layer.msg('重置登录密码,资金密码成功');
                        setTimeout(function () {
                            location.href = "login.html";
                        }, 2000);
                    });
            }
        });
    });
});
